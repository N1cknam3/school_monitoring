<?php

App::uses('AppController', 'Controller');

class UsersController extends AppController {
	
	//проверка, находится ли пользователь в системе,
	//и на то, соответствует ли этот тип нужному
	protected function is_login($neededUserType = null) {
		
		$user = $this->Session->read('userID');
		
		if ($neededUserType == null) {
			//если не проверяем доступ, то смотрим просто на наличие сессии
			return !empty($user);
	        
		} else {				
			
	        if (!empty($user)) {
	            return $this->is_correctUser($neededUserType);
	        }		
	        else {
	        	return false;
	        }	
	        
        }
	}	
	
	//проверка полномочий пользователя
	protected function is_correctUser($neededUserType) {
		$userType = $this->Session->read('type');
		
		if (empty($userType)) {
			$this->logout();
		} else {
			if ($userType != $neededUserType) {
				return false;
			} else {
				return true;
			}
		}
	}
	
	protected function clearSessionData() {
		$this->Session->write('userID', '');
		$this->Session->write('type', '');
	}
	
	public function logout() {
		$this->clearSessionData();
		$this->redirect('/');
	}
	
	public function get_user_byLoginPass($login_to_check, $pass_to_check) {
		$this->loadModel('User');
		$user = $this->User->find('first', array('conditions' => array('login' => $login_to_check, 'pass' => $pass_to_check)));
		return $user;
	}
	
	public function get_user_byID($id) {
		$this->loadModel('User');
		$user = $this->User->find('first', array('conditions' => array('id' => $id)));
		return $user;
	}
	
	//******************************************//
	//***************РАБОТА С БД****************//
	//******************************************//
	
	protected function getPaginate($modelName) {
		$this->loadModel($modelName);
		$data = $this->paginate($modelName);
		return $data;
	}
	
	protected function checkAnketToSemester($anket, $semester) {
		if (($semester['Semester']['date_start'] <= $anket['date']) && ($semester['Semester']['date_end'] >= $anket['date']))
			return true;
		else
			return false;
	}
	
	protected function checkSemester($semester) {
		$today = date("Y-m-d");

		if (!empty($semester)) {
		
			if (($semester['Semester']['date_start'] <= $today) && ($semester['Semester']['date_end'] >= $today))
				return true;

		}
		return false;

	}
	
	protected function getLastSemester($number = null) {
		$this->loadModel('Semester');
		
		if ($number == null) {
			$semesters = $this->Semester->find('first', array('order' => array('Semester.date_start' => 'DESC')));	

		} else {
			$semesters = $this->Semester->find('all', array(
				'order' => array(
					'Semester.date_start' => 'DESC'
				),
				'limit' => $number
			));
		}

		return $semesters;
	}
	
	protected function getLastAnket($pupil_id) {
		$this->loadModel('Anket');
		
		$anket = $this->Anket->find('first', array('conditions' => array('pupil_id' => $pupil_id), 'order' => array('Anket.id' => 'DESC')));

		return $anket;
	}
	
	protected function getPupils($class_id) {
		$this->loadModel('Pupil');
		
		$pupils = $this->Pupil->find('all', array('conditions' => array('class_id' => $class_id), 'order' => array('name_1' => 'ASC', 'name_2' => 'ASC', 'name_3' => 'ASC')));
		
		return $pupils;
	}
	
	protected function getClass($teacher_id) {
		$this->loadModel('MyClass');
		
		$class = $this->MyClass->find('first', array('conditions' => array('teacher_id' => $teacher_id), array ('order' => array('number' => 'ASC', 'letter' => 'ASC'))));
		
		if (empty($class))
			return $class;
		else
			return $class['MyClass'];
	}
	
	protected function getTeacher($user_id) {
		$this->loadModel('Teacher');
		
		$teacher = $this->Teacher->find('first', array('conditions' => array('user_id' => $user_id)));
		
		if (empty($teacher))
			return $teacher;
		else
			return $teacher['Teacher'];
	}
	
	protected function getUser($user_id) {
		$this->loadModel('User');
		
		$user = $this->User->find('first', array('conditions' => array('User.id' => $user_id)));
		
		if (empty($user))
			return $user;
		else
			return $user['User'];
	}
	
	protected function getElementFromModel($table, $elem_id) {
		$this->loadModel($table);
		
		$answer = $this->$table->find('first', array('conditions' => array($table.'.id' => $elem_id)));
		
		return $answer;
	}
	
	protected function getFreeTeachers() {
		$this->loadModel('Teacher');
		$this->loadModel('MyClass');
		
		$classes = $this->MyClass->find('all');
		$teachers = $this->Teacher->find('all', array ('order' => array('name_1' => 'ASC', 'name_2' => 'ASC', 'name_3' => 'ASC')));
		
		$answer = array();
		
		foreach ($teachers as $teacher) {
			$class_count = 0;
			foreach ($classes as $class) {			
				if ($class['MyClass']['teacher_id'] == $teacher['Teacher']['id']) {
					$class_count++;
					break;
				}
			}
			if ($class_count == 0) {
				array_push($answer, $teacher);
			}
		}
		
		return $answer;
	}

	public function removePupilClass($pupil_id) {
		$this->loadModel('Pupil');
		$this->Pupil->id = $pupil_id;
		if (!$this->Pupil->exists()) {
			throw new NotFoundException(__('Не найден ученик с ID='.$mark_id));
		}
		$data = array(
			'Pupil' => array(
				'class_id' => null
			)
		);
		if ($this->Pupil->save($data)) {
			
		} else {
			$this->Session->setFlash(__('Класс ученика не удален. Попробуйте еще раз.'));
		}
	}
	
	protected function getSubjectsTypes() {
		$this->loadModel('Type');
		
		$types = $this->Type->find('all', array('order' => array('Type.name' => 'ASC')));
		
		return $types;
	}
	
	protected function deleteElementFromModel($elem_id, $modelName) {
		$this->loadModel($modelName);
		$this->$modelName->id = $elem_id;
		if (!$this->$modelName->exists()) {
			throw new NotFoundException(__('Не найден '.$modelName.' с ID='.$elem_id));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->$modelName->delete()) {
			$this->Session->setFlash(__($modelName.' удален.'));
		} else {
			$this->Session->setFlash(__($modelName.' не удален. Попробуйте еще раз.'));
		}
	}
	
}

?>