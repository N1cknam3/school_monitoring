<?php

App::uses('UsersController', 'Controller');
App::uses('AppController', 'Controller');

class LoginsController extends UsersController {
	
	const ERR_ALL_FIELDS = 'Все поля обязательны для заполнения!';
	const ERR_LOGIN_PASS = "Неверное имя пользователя или пароль!";
	const ERR_FILE = "Недопустимый тип файла!";
	const SUCCESS_UPDATE = "Данные успешно сохранены!";
	const SUCCESS_FILE = "Файлы успешно загружены";
	const ERR_FILE_DEL = "Ошибка удаления файла";
	
	public $name = 'Logins';
	public $helpers = array('Html', 'Session');
	public $uses = array();
	
	public $hasLogin = false;
	
	public function beforeFilter() {
		if ( !$this->request->is('ajax')) {
			if ($this->is_login()) {
				//если пользователь вошел в систему, но попал в окно авторизации, то перенаправляем его к своему кабинету
				if ($this->is_correctUser('1')) {
					$this->redirect(array('controller' => 'admins', 'action' => 'cabinet'));
				} else {
					$this->redirect(array('controller' => 'teachers', 'action' => 'cabinet'));
				}
			}
		}
	}
	
	public function start(){
		$this->layout = 'startlayout';
		$this->set('title_for_layout', 'Главная');
	}
	
	public function ajax_login(){
		$this->layout = 'ajax';
		if(empty($this->request->data['login'])||empty($this->request->data['pass'])) {
			$this->set('login_message', self::ERR_ALL_FIELDS);
		} else {
			
			$user = $this->get_user_byLoginPass($this->request->data['login'], $this->request->data['pass']);
			
			if (!empty($user)) {
				if ($user['User']['type'] == '1') {				
					$this->setSessionData($user['User']['id'], '1');
					$this->set('login_message', 'ok');
					$this->set('login_url', Router::fullbaseUrl().Router::url(array('controller' => 'admins', 'action' => 'cabinet')));
				} else if ($user['User']['type'] == '2') {
					$this->setSessionData($user['User']['id'], '2');
					$this->set('login_message', 'ok');
					$this->set('login_url', Router::fullbaseUrl().Router::url(array('controller' => 'teachers', 'action' => 'cabinet')));
				}
			} else {
				$this->set('login_message', self::ERR_LOGIN_PASS);
			}
		}		
	}
	
	private function setSessionData($loginToWrite, $typeToWrite = null) {
		$this->Session->write('userID', $loginToWrite);
		//1=admin
		//2=teacher
		$this->Session->write('type', $typeToWrite);
	}
	
	private function check_user($login_to_check, $pass_to_check, $type_to_check = null) {
		$this->loadModel('User');
		$user = $this->User->find('first', array('conditions' => array('login' => $login_to_check, 'pass' => $pass_to_check)));
		
		if (empty($user)) {
			return false;
		} else {
			
			if ($type_to_check != null) {
				if ($user['User']['type'] != $type_to_check) {
					return false;
				}
			}
			
			return true;
		}
	}
	
}

?>