<?php

App::uses('UsersController', 'Controller');
App::uses('AppController', 'Controller');

class TeachersController extends UsersController {

	public $name = 'Teachers';
	public $helpers = array('Html', 'Session', 'Widgets');
	public $uses = array();
	
	public function beforeRender() {
		if ($this->is_login()) {
			//если пользователь вошел в систему, но попал в окно авторизации, то перенаправляем его к своему кабинету
			if ($this->is_correctUser('2')) {
				$this->layout = 'teacherlayout';
			} else {
				$this->redirect('/');
			}
		}
	}
	
	public function cabinet() {
		
		$this->set('title_for_layout', 'Кабинет учителя');
		
		//получение списка учеников
		$user = $this->getUser($this->Session->read('userID'));
		$teacher = $this->getTeacher($user['id']);
		$this->set('teacher', $teacher);
		$class = $this->getClass($teacher['id']);
		$this->set('class', $class);
		if (!empty($class))
			$pupils = $this->getPupils($class['id']);
		else
			$pupils = array();

		$lastSemester = $this->getLastSemester();
		//проверка существования активного семестра
		if ($this->checkSemester($lastSemester)) {
			$this->set('semesterIsActive', true);
		} else {
			$this->set('semesterIsActive', false);
		}
		
		$pupilsWithAnkets = array();
		foreach ($pupils as $pupil) {
			$lastAnket = $this->getLastAnket($pupil['Pupil']['id']);
			if (empty($lastAnket)) {
				//Вывод ученика с кнопкой "Добавление" и красным цветом
				//так как у него нет ни одной анкеты
				$pupil['Pupil']['anket'] = '';
			} else {
				//Проверяем состояние этой анкеты
				if ($this->checkAnketToSemester($lastAnket['Anket'], $lastSemester)) {
					//Вывод ученика с кнопкой "Редактировать" и "Удалить" и зеленым цветом
					//так как у него есть активная анкета
					$lastAnket['Anket']['state'] = 'new';
					$pupil['Pupil']['anket'] = $lastAnket['Anket'];
				} else {
					//Вывод ученика с кнопкой "Добавление" и "Редактировать" и желтым цветом
					//так как у него есть только устаревшая анкета
					$lastAnket['Anket']['state'] = 'old';
					$pupil['Pupil']['anket'] = $lastAnket['Anket'];
				}
			}
			array_push($pupilsWithAnkets, $pupil);
		}
		
		//В этом списке передаем всех учеников и последние их анкеты, если таковые есть, с указанием состояния этой анкеты для последнего учебного периода
		$this->set('pupils', $pupilsWithAnkets);
		
	}
	
	//******************************************//
	//*****РАБОТА С ОПЕРАЦИЯМИ НАД АНКЕТАМИ*****//
	//******************************************//
	
	public function addAnket($pupil_id) {
		if ($pupil_id != null) {
			
			$this->set('title_for_layout', 'Создание анкеты');
			
			$this->set('formAction', 'addAnket');
			
			//Получаем все необходимые данные для вывода
			$pupil = $this->getElementFromModel('Pupil', $pupil_id);
			$this->set('pupil', $pupil);
			
			//получаем год обучения класса (номер), чтобы выбрать необходимые предметы
			$this->loadModel('MyClass');
			$class_number = $this->MyClass->find('first', array('conditions' => array('MyClass.id' => $pupil['Pupil']['class_id'])));	
			$class_number = $class_number['MyClass']['number'];
			
			$subject_types = $this->getSubjectsTypes();			
			
			$this->loadModel('Subject');
			$types_with_all_relative_subjects = array();
			foreach ($subject_types as $type) {
				$type['Type']['subjects'] = $this->Subject->find('all', array('conditions' => array('type_id' => $type['Type']['id'], 'start_study_year <=' => $class_number, 'end_study_year >=' => $class_number)));					
				array_push($types_with_all_relative_subjects, $type);
			}
			$this->set('subject_types', $types_with_all_relative_subjects);

			//	Загрузка личностных данных
			$this->loadModel('Personal_data_field');
			$personal_data = $this->Personal_data_field->find('all');
			$this->set('personal_data', $personal_data);

			$this->loadModel('Personal_data_answer');
			$personal_data_answers = $this->Personal_data_answer->find('list');
			$this->set('personal_data_answers', $personal_data_answers);

			$this->set('editMode', false);
			
			if (!empty($this->request->data)) {
				//Необходимо сохранить полученные данные в БД
				
				//назначение активного семестра этой анкете
				$lastSemester = $this->getLastSemester();

				//Создаем для начала новую анкету
				$teacher = $this->getTeacher($this->Session->read('userID'));
				$anketData = array(
					'pupil_id' => $pupil_id,
					'teacher_id' => $teacher['id'],
					'semester_id' => $lastSemester['Semester']['id'],
					'date' => date("Y-m-d"),
					'mark_complex' => $this->request->data['Teacher']['mark_complex']
				);

				$this->loadModel('Anket');
				if($this->Anket->save($anketData)){
					$anketID = $this->Anket->id;
				}
				
				//Далее создаем все записи для оценок в таблице оценок				
				foreach($this->request->data['Teacher'] as $key => $value) {
					$subject_id = str_replace('subject-', '', $key);
					//так как в $key скрывается id предмета, то необходимо из полей взять только числовые
					if (is_numeric($subject_id)) {						
						$this->loadModel('Mark');
						$this->Mark->create();
						$markData = array(
							'mark' => $value,
							'anket_id' => $anketID,
							'subject_id' => $subject_id
						);
						
						$this->Mark->save($markData);
					}
				}

				//Далее создаем все записи для уровня личностных достижений в соответствующей таблице				
				foreach($this->request->data['Teacher'] as $key => $value) {
					$personal_id = str_replace('personal-', '', $key);
					//так как в $key скрывается id, то необходимо из полей взять только числовые
					if (is_numeric($personal_id)) {						
						$this->loadModel('Personal_data_anket');
						$this->Personal_data_anket->create();
						$personalData = array(
							'anket_id' => $anketID,
							'personal_data_field_id' => $personal_id,
							'personal_data_answer_id' => $value
						);
						
						$this->Personal_data_anket->save($personalData);
					}
				}
				
				//Теперь перейдем из формы добавления в форму редактирования только что созданной анкеты этого ученика
				$this->redirect(array('controller' => 'teachers', 'action' => 'cabinet'));
			}
		}
	}
	
	public function editAnket($pupil_id, $anket_id) {
		if (($pupil_id == null) || ($anket_id == null)) {
			
		} else {
			
			$this->set('title_for_layout', 'Редактирование анкеты');
			
			$this->set('formAction', 'editAnket');
			$this->set('anket_id', $anket_id);
			
			//Получаем все необходимые данные для вывода
			$pupil = $this->getElementFromModel('Pupil', $pupil_id);
			$this->set('pupil', $pupil);
			
			$subject_types = $this->getSubjectsTypes();			
			
			$this->loadModel('Subject');
			$types_with_all_relative_subjects = array();
			foreach ($subject_types as $type) {
				$type['Type']['subjects'] = $this->Subject->find('all', array('conditions' => array('type_id' => $type['Type']['id'])));

				$subjects_with_marks = array();
				foreach($type['Type']['subjects'] as $subject) {
					
					$this->loadModel('Mark');
					$finded_mark = $this->Mark->find('first', array('conditions' => array('anket_id' => $anket_id, 'subject_id' => $subject['Subject']['id'])));

					if (!empty($finded_mark)) {
						$subject['Subject']['mark'] = $finded_mark['Mark']['mark'];
					}
					array_push($subjects_with_marks, $subject);
				}
				$type['Type']['subjects'] = $subjects_with_marks;
				
				array_push($types_with_all_relative_subjects, $type);
			}
			$this->set('subject_types', $types_with_all_relative_subjects);
			
			$this->loadModel('Anket');
			$anket = $this->Anket->find('first',
				array(
					'conditions' => array('Anket.id' => $anket_id),
					'contain' => array(
						'Pupil',
						'Semester',
						'Mark',
						'Personal_data_anket' => array(
							'Personal_data_field',
							'Personal_data_answer'
						)						
					)

				)
			);
			$this->set('anket', $anket);

			//	Загрузка личностных данных
			$this->loadModel('Personal_data_field');
			$personal_data = $this->Personal_data_field->find('all');
			$this->set('personal_data', $personal_data);

			$this->loadModel('Personal_data_answer');
			$personal_data_answers = $this->Personal_data_answer->find('list');
			$this->set('personal_data_answers', $personal_data_answers);

			$this->set('editMode', true);			
			
			if (!empty($this->request->data)) {
				//Необходимо сохранить полученные данные в БД
				
				//Обновляем все записи оценок в таблице оценок				
				foreach($this->request->data['Teacher'] as $key => $value) {
					$subject_id = str_replace('subject-', '', $key);
					//так как в $key скрывается id предмета, то необходимо из полей взять только числовые
					if (is_numeric($subject_id)) {						
						$this->loadModel('Mark');
						$current_mark = $this->Mark->find('first', array('conditions' => array('anket_id' => $anket_id, 'subject_id' => $subject_id)));
						if (!empty($current_mark))
							$this->Mark->id = $current_mark['Mark']['id'];
						else {
							$this->Mark->create();
						}
						$markData = array(
							'mark' => $value,
							'anket_id' => $anket_id,
							'subject_id' => $subject_id
						);
						
						$this->Mark->save($markData);
					}
				}

				//Далее обновляем все записи для уровня личностных достижений в соответствующей таблице				
				foreach($this->request->data['Teacher'] as $key => $value) {
					$personal_field_id = str_replace('personal-', '', $key);
					//так как в $key скрывается id, то необходимо из полей взять только числовые
					if (is_numeric($personal_field_id)) {						
						$this->loadModel('Personal_data_anket');
						$current_personal_anket = $this->Personal_data_anket->find('first', array('conditions' => array('anket_id' => $anket_id, 'personal_data_field_id' => $personal_field_id)));
						if (!empty($current_personal_anket))
							$this->Personal_data_anket->id = $current_personal_anket['Personal_data_anket']['id'];
						else {
							$this->Personal_data_anket->create();
						}
						$personalData = array(
							'anket_id' => $anket_id,
							'personal_data_field_id' => $personal_field_id,
							'personal_data_answer_id' => $value
						);
						
						$this->Personal_data_anket->save($personalData);
					}
				}
				
				//Теперь перейдем из формы добавления в форму редактирования только что созданной анкеты этого ученика
				$this->redirect(array('controller' => 'teachers', 'action' => 'editAnket/'.$pupil_id.'/'.$anket_id));
			}
		}
	}
	
	public function showAnket($anket_id = null) {
		if ($pupil_id != null) {
			
			$this->set('title_for_layout', 'Просмотр анкеты');
			
		}
	}
	
	public function deleteAnket($anket_id = null) {
		if ($anket_id != null) {
			
			$this->loadModel('Anket');
			$this->Anket->id = $anket_id;
			if (!$this->Anket->exists()) {
				throw new NotFoundException(__('Не найдена анкета с ID='.$anket_id));
			}
			$this->request->allowMethod('post', 'delete');
			if ($this->Anket->delete()) {
				$this->Session->setFlash(__('Анкета удалена.'));
			} else {
				$this->Session->setFlash(__('Анкета не удалена. Попробуйте еще раз.'));
			}
			
			//Плюс к этому необходимо удалить и все записи в остальных таблицах, связанные с этой анкетой
			//Удаляем все оценки
			$this->loadModel('Mark');
			$marks_related_to_anket = $this->Mark->find('all', array('conditions' => array ('anket_id' => $anket_id)));
			
			foreach($marks_related_to_anket as $mark) {
				$mark_id = $mark['Mark']['id'];
				$this->Mark->id = $mark_id;
				if (!$this->Mark->exists()) {
					throw new NotFoundException(__('Не найдена оценка с ID='.$mark_id));
				}
				if ($this->Mark->delete()) {
					
				} else {
					$this->Session->setFlash(__('Оценка не удалена. Попробуйте еще раз.'));
				}
			}
			
			return $this->redirect(array('action' => 'cabinet'));
		}
	}

	public function chooseClass() {
		if (!empty($this->request->data)) {
			$class_id = $this->request->data['Teacher']['class_id'];
			//	TODO: Если неверный ID то ошибку
			$this->redirect(array('controller' => 'teachers', 'action' => 'chooseWorkTable/'.$class_id));
		}

		$this->set('title_for_layout', 'Выбор класса');

		$formAction = 'chooseClass';
		$this->set('formAction', $formAction);

		$this->loadModel('MyClass');
		$classes = $this->MyClass->find('all');

		$classes_with_teachers = array();
		foreach ($classes as $class) {
			$curr_class[$class['MyClass']['id']] = $class['MyClass']['number'].$class['MyClass']['letter'] . " (" .$class['Teacher']['name_1']." ".$class['Teacher']['name_2']." ".$class['Teacher']['name_3']. ")";
			$classes_with_teachers += $curr_class;
		}
		$this->set('classes_data', $classes_with_teachers);
	}

	public function chooseWorkTable($class_id) {
		$this->loadModel('MyClass');
		$classes = $this->MyClass->find('first',
			array (
				'conditions' => array('MyClass.id' => $class_id),
				'contain' => array(
					'WorkTable'
				)
			)
		);
		$this->set('class_id', $class_id);
		$this->set('classes', $classes);
	}

	public function addWorkTable($class_id) {

		if (!empty($this->request->data)) {
			$name = trim($this->request->data['Teacher']['name']);
			$count_easy = $this->request->data['Teacher']['count_easy'];
			$count_middle = $this->request->data['Teacher']['count_middle'];
			$count_difficult = $this->request->data['Teacher']['count_difficult'];

			if (!empty($name) && is_numeric($count_easy) && is_numeric($count_middle) && is_numeric($count_difficult)) {

				$this->loadModel('WorkTable');
				$this->WorkTable->create();
				$this->WorkTable->save(['class_id' => $class_id, 'name' => $name]);

				$work_table_id = $this->WorkTable->id;

				$this->loadModel('Task');
				for($i=0; $i < $count_easy; $i++) {
					$this->Task->create();
					$this->Task->save(['work_table_id' => $work_table_id, 'level' => 0]);
				}
				for($i=0; $i < $count_middle; $i++) {
					$this->Task->create();
					$this->Task->save(['work_table_id' => $work_table_id, 'level' => 1]);
				}
				for($i=0; $i < $count_difficult; $i++) {
					$this->Task->create();
					$this->Task->save(['work_table_id' => $work_table_id, 'level' => 2]);
				}

				$this->redirect(array('controller' => 'teachers', 'action' => 'chooseWorkTable/'.$class_id));
			}
		}

		$this->set('title_for_layout', 'Добавление новой работы');
		$this->set('formAction', 'addWorkTable/'.$class_id);
		$this->set('returnAction', 'chooseWorkTable/'.$class_id);
	}

	//	TODO: $work_table_id вместо $class_id
	public function workTable($work_table_id) {

		$this->set('title_for_layout', 'Редактирование таблицы');

		$anotherAction = 'tableSkills/'.$work_table_id;
		$this->set('anotherAction', $anotherAction);

		$formAction = 'workTable/'.$work_table_id;
		$this->set('formAction', $formAction);

		$this->loadModel('WorkTable');
		$work_table = $this->WorkTable->find('first',
			array (
				'conditions' => array('WorkTable.id' => $work_table_id),
				'contain' => array(
					'MyClass' => array(
						'Pupil' => array(
							'PupilTask' => array(
								'order' => ['task_id'],
								'conditions' => ['work_table_id' => $work_table_id]
							),
							'PupilResult' => array(
								'conditions' => ['work_table_id' => $work_table_id]
							)
						)
					),
					'Task' => array(
						'order' => ['id']
					)
				)
			)
		);
		$this->set('work_table', $work_table);

		if (!empty($this->request->data)) {
			$data = $this->request->data;

			$result = $this->getResult($data, $work_table['Task']);

			$this->loadModel('PupilResult');
			foreach ($result as $pupil_id => $workResult) {
				$options = [
					'work_table_id' => $work_table_id,
					'pupil_id' => $pupil_id
				];

				$current = $this->PupilResult->find('first', array('conditions' => $options));
				if ($current)
					$this->PupilResult->id = $current['PupilResult']['id'];
				else
					$this->PupilResult->create();

				$options += $workResult;

				$this->PupilResult->save($options);
			}

			$this->loadModel('Task');
			foreach ($data['mark'] as $level => $task) {
				foreach ($task as $task_id => $value) {
					$current = $this->Task->find('first', array('conditions' => ['id' => $task_id]));
					if ($current)
						$this->Task->id = $current['Task']['id'];
					else
						$this->Task->create();

					$value = is_numeric($value) ? $value : 0;
					$this->Task->save(['work_table_id' => $work_table_id, 'level' => $level, 'mark' => $value]);
				}
			}
			$this->set('data', $data);
			unset($data['mark']);

			$this->loadModel('PupilTask');
			foreach ($data as $pupil_id => $pupil_marks) {
				foreach ($pupil_marks as $task_level => $tasks) {
					foreach ($tasks as $task_id => $value) {
						$current = $this->PupilTask->find('first', array('conditions' => ['pupil_id' => $pupil_id, 'task_id' => $task_id]));
						if ($current)
							$this->PupilTask->id = $current['PupilTask']['id'];
						else
							$this->PupilTask->create();

						$value = is_numeric($value) ? $value : 0;
						$this->PupilTask->save(['pupil_id' => $pupil_id, 'task_id' => $task_id, 'work_table_id' => $work_table_id, 'mark' => $value]);
					}
				}
			}

			$this->redirect(array('controller' => 'teachers', 'action' => 'workTable/'.$work_table_id));
		}
	}

	public function tableSkills($class_id) {

		$this->set('title_for_layout', 'Редактирование таблицы');

		$anotherAction = 'table/'.$class_id;
		$this->set('anotherAction', $anotherAction);

		$formAction = 'table/'.$class_id;
		$this->set('formAction', $formAction);

		$this->loadModel('MyClass');
		$this->MyClass->recursive = 1;
		$classes = $this->MyClass->find('all',
			array (
				'conditions' => array('MyClass.id' => $class_id),
				'contain' => array(
					'Pupil' => array(
						'order' => 'Pupil.name_1 ASC'
					)
				)
			)
		);

		if (!empty($this->request->data)) {
			$data = $this->request->data;
			$this->set('maxMarks', $data['mark']);
			
			$result = $this->getResult($data);
			//	TODO: потом когда все будет храниться в таблице, переделать
			foreach ($classes[0]['Pupil'] as $key => $value) {
				$value['result'] = $result[$value['id']];
				$value['marks'] = $data[$value['id']];
				$classes[0]['Pupil'][$key] = $value;
			}
		}

		$this->set('classes', $classes);
	}

	private function getResult($requestData, $tasks) {
		$maxWorkMark = 0;
		$maxWorkMarkEasyMiddle = 0;
		$maxWorkMarkDifficult = 0;

		foreach ($requestData['mark'] as $level => $task) {
			foreach ($task as $task_id => $value) {
				$maxWorkMark += $value;

				if (($level == 0) || ($level == 1))
					$maxWorkMarkEasyMiddle++;
				if ($level == 2)
					$maxWorkMarkDifficult++;
			}
		}

		unset($requestData['mark']);

		$answer = [];
		foreach ($requestData as $pupil_id => $pupilWork) {
			$pupilMark = 0;
			$pupilMarkEasyMiddle = 0;
			$pupilMarkDifficult = 0;

			foreach ($pupilWork as $task_level => $task) {
				foreach ($task as $task_id => $mark) {
					$pupilMark += $mark;

					foreach ($tasks as $taskRecord) {
						if ($taskRecord['id'] == $task_id) {

							if (($task_level == 0) || ($task_level == 1)) {
								if ($taskRecord['mark'] == $mark)
									$pupilMarkEasyMiddle++;
							}
							if ($task_level == 2) {
								if ($taskRecord['mark'] == $mark)
									$pupilMarkDifficult++;
							}

							break;
						}
					}
				}
			}

			$success_percent_e_m = round($pupilMarkEasyMiddle / $maxWorkMarkEasyMiddle * 100);
			$success_percent_d = round($pupilMarkDifficult / $maxWorkMarkDifficult * 100);
			$success_percent = round($pupilMark / $maxWorkMark * 100);

			$answer[$pupil_id] = [
				'count_e_m' => $pupilMarkEasyMiddle,
				'success_percent_e_m' => $success_percent_e_m,
				'count_d' => $pupilMarkDifficult,
				'success_percent_d' => $success_percent_d,
				'score' => $pupilMark,
				'success_percent' => $success_percent
			];
		}
		return $answer;
	}
}

?>