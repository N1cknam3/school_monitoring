<?php

App::uses('UsersController', 'Controller');
App::uses('AppController', 'Controller');

class AdminsController extends UsersController {

	public $name = 'Admins';
	public $helpers = array('Html', 'Session', 'Paginator');
	public $uses = array();
	
	var $paginate = array();
	
	public function beforeFilter() {
		if ($this->is_login()) {
			//если пользователь вошел в систему, но попал в окно авторизации, то перенаправляем его к своему кабинету
			if ($this->is_correctUser('1')) {
				$this->layout = 'adminlayout';
			} else {
				$this->redirect('/');
			}
		} else {
			$this->redirect('/');
		}
	}
	
	public function cabinet() {
		$this->set('title_for_layout', 'Кабинет завуча');

		$this->loadModel('Admin');
		$admin = $this->Admin->find('first', array('conditions' => array('id' => $this->Session->read('userID'))));
		$this->set('admin_data', $admin);
	}
	
	//****************************************//
	//****************СЕМЕСТРЫ****************//
	
	public function semester_list() {
		$this->set('title_for_layout', 'Список учебных четвертей');
		
		$this->paginate = array(
			'limit' => 25,
	        'order' => array(
	            'Semester.date_start' => 'desc'
	        )
	    );
	    
	    $last_semester = $this->getLastSemester();
	    $this->set('last_semester', $last_semester);

	    if ($this->checkSemester($last_semester)) {
			$this->set('semesterIsActive', true);
		} else {
			$this->set('semesterIsActive', false);
		}
		
		$this->set('semester_data', $this->getPaginate('Semester'));
	}
	
	public function editSemester($semester_id = null) {
		
		//Сохранение введенных данных
		if (!empty($this->request->data)) {
			$this->loadModel('Semester');
			$this->Semester->id = $semester_id;
			$data = array(
				'date_start' => $this->request->data['Admin']['date_start'],
				'date_end' => $this->request->data['Admin']['date_end']
			);
			$this->Semester->save($data);
			
			$this->redirect(array('controller' => 'admins', 'action' => 'semester_list'));
		}
		
		$this->set('title_for_layout', 'Изменение четверти');		
		$this->set('formAction', 'editSemester');
		
		//Загрузка данных
		$this->loadModel('Semester');
		$semester = $this->Semester->find('first', array('conditions' => array('id' => $semester_id)));
		$this->set('semester', $semester);
		
	}
	
	public function addSemester($semester_id = null) {
		
		//Сохранение введенных данных
		if (!empty($this->request->data)) {
			$this->loadModel('Semester');
			$this->Semester->create();
			$data = array(
				'date_start' => $this->request->data['Admin']['date_start'],
				'date_end' => $this->request->data['Admin']['date_end']
			);
			$this->Semester->save($data);

			$this->redirect(array('controller' => 'admins', 'action' => 'semester_list'));
		}
		
		$this->set('title_for_layout', 'Создание новой четверти');		
		$this->set('formAction', 'addSemester');
		
		$last_semester = $this->getLastSemester();
		
		$today1 = date("Y");
		$today2 = date("m-d");
		
		//задание значений по-умолчанию для периода новой четверти
		if (($today2 >= '06-01')&&($today2 <= '10-31')) {
			//1 четверть
			$date_start = new DateTime($today1.'-09-01');
			$date_end = new DateTime($today1.'-10-31');
		} else if (($today2 >= '11-01')&&($today2 <= '12-31')) {
			//2 четверть
			$date_start = new DateTime($today1.'-11-01');
			$date_end = new DateTime($today1.'-12-31');
		} else if (($today2 >= '01-01')&&($today2 <= '03-25')) {
			//3 четверть
			$date_start = new DateTime($today1.'-01-12');
			$date_end = new DateTime($today1.'-03-20');
		} else if (($today2 >= '03-26')&&($today2 <= '05-31')) {
			//4 четверть
			$date_start = new DateTime($today1.'-04-01');
			$date_end = new DateTime($today1.'-05-30');
		};
		
		$start_semester_data = array(
			'Semester' => array(
				'date_start' => $date_start->format('Y-m-d'),
				'date_end' => $date_end->format('Y-m-d')
			)
		);
		
		$this->set('semester', $start_semester_data);

	}
	
	public function deleteSemester($semester_id = null) {
		if ($semester_id != null) {
			$this->deleteElementFromModel($semester_id, 'Semester');
			return $this->redirect(array('action' => 'semester_list'));
		}		
	}
	
	//****************СЕМЕСТРЫ****************//
	//****************************************//
	
	//$%^&&^%*(*@&$%*(&#&)%&(^#&*%)&*)#^()&^%(^)&*#%*(&^$^@(&*%$&@^$%*^(@)_)*%&^@^(&$)_@*&$^^#%$%^@$&)*(*(*%&^^*@&#$(*&)&(@*&^)&$%//
	//$%^&&^%*(*@&$%*(&#&)%&(^#&*%)&*)#^()&^%(^)&*#%*(&^$^@(&*%$&@^$%*^(@)_)*%&^@^(&$)_@*&$^^#%$%^@$&)*(*(*%&^^*@&#$(*&)&(@*&^)&$%//
	//$%^&&^%*(*@&$%*(&#&)%&(^#&*%)&*)#^()&^%(^)&*#%*(&^$^@(&*%$&@^$%*^(@)_)*%&^@^(&$)_@*&$^^#%$%^@$&)*(*(*%&^^*@&#$(*&)&(@*&^)&$%//
	//$%^&&^%*(*@&$%*(&#&)%&(^#&*%)&*)#^()&^%(^)&*#%*(&^$^@(&*%$&@^$%*^(@)_)*%&^@^(&$)_@*&$^^#%$%^@$&)*(*(*%&^^*@&#$(*&)&(@*&^)&$%//
	
	//**************************************//
	//****************КЛАССЫ****************//
	
	public function class_list() {		
		$this->loadModel('MyClass');	

		$this->MyClass->recursive = 2;

		$classes = $this->MyClass->find('all');
		$this->set('class_data', $classes);
		
		$this->set('title_for_layout', 'Список классов');
	}
	
	public function addClass() {
		if (!empty($this->request->data)) {
			$this->loadModel('MyClass');
			$this->MyClass->create();
			$data = array(
				'MyClass' => array(
						'teacher_id' => $this->request->data['Admin']['teacher_id'],
						'number' => $this->request->data['Admin']['number'],
						'letter' => $this->request->data['Admin']['letter']
				)
			);
			$this->MyClass->save($data);

			$this->redirect(array('controller' => 'admins', 'action' => 'class_list'));
		}
		
		$teachers = $this->getFreeTeachers('list');
		$teachers_data = array();
		foreach ($teachers as $teacher) {
			$teachers_data[$teacher['Teacher']['id']] = $teacher['Teacher']['name_1'] ." ". $teacher['Teacher']['name_2'] ." ". $teacher['Teacher']['name_3'];
		}
		$this->set('teachers_data', $teachers_data);
		$this->set('formAction', 'addClass');
		$this->set('title_for_layout', 'Создание класса');
	}
	
	public function editClass($class_id = null) {
		if ($class_id != null) {
			if (!empty($this->request->data)) {
				$this->loadModel('MyClass');
				$this->MyClass->id = $class_id;
				$data = array(
					'MyClass' => array(
						'teacher_id' => $this->request->data['Admin']['teacher_id'],
						'number' => $this->request->data['Admin']['number'],
						'letter' => $this->request->data['Admin']['letter']
				));
				$this->MyClass->save($data);

				$this->redirect(array('controller' => 'admins', 'action' => 'class_list'));
			}

			//ищем всех свободных учителей
			$teachers = $this->getFreeTeachers('list');

			//но также нужно брать и учителя этого класса
			$curr_class = $this->MyClass->find('first', array('conditions' => array('MyClass.id' => $class_id)));
			$this->loadModel('Teacher');
			$curr_class_teacher = $this->Teacher->find('first', array('conditions' => array('id' => $curr_class['MyClass']['teacher_id'])));
			array_push($teachers, $curr_class_teacher);			

			$teachers_data = array();
			foreach ($teachers as $teacher) {
				$teachers_data[$teacher['Teacher']['id']] = $teacher['Teacher']['name_1'] ." ". $teacher['Teacher']['name_2'] ." ". $teacher['Teacher']['name_3'];
			}
			$this->set('teachers_data', $teachers_data);
			$this->set('current_teacher', $curr_class_teacher['Teacher']['id']);
			$this->set('class_data', $curr_class);
			$this->set('formAction', 'editClass');
			$this->set('title_for_layout', 'Изменение класса');
		} else {
			$this->redirect(array('controller' => 'admins', 'action' => 'class_list'));
		}
	}
	
	public function deleteClass($class_id = null) {
		if ($class_id != null) {
			$this->deleteElementFromModel($class_id, 'MyClass');

			//Теперь необходимо найти всех учеников данного класса и убрать их принадлежность к этому класссу
			$pupils = $this->getPupils($class_id);
			if (!empty($pupils)) {
				foreach ($pupils as $pupil) {	
					$this->removePupilClass($pupil['Pupil']['id']);
				}
			}

			return $this->redirect(array('action' => 'class_list'));
		} else {
			$this->redirect(array('controller' => 'admins', 'action' => 'class_list'));
		}
	}
	
	public function showClass($class_id = null) {
		if ($class_id != null) {
			$this->set('title_for_layout', 'Просмотр класса');

			$this->loadModel('MyClass');

			$this->MyClass->recursive = 2;

			$class = $this->MyClass->find('first', array('conditions' => array('MyClass.id' => $class_id)));
			$this->set('class', $class);
			
		} else {
			$this->redirect(array('controller' => 'admins', 'action' => 'class_list'));
		}
	}
	
	//****************КЛАССЫ****************//
	//**************************************//
	
	//$%^&&^%*(*@&$%*(&#&)%&(^#&*%)&*)#^()&^%(^)&*#%*(&^$^@(&*%$&@^$%*^(@)_)*%&^@^(&$)_@*&$^^#%$%^@$&)*(*(*%&^^*@&#$(*&)&(@*&^)&$%//
	//$%^&&^%*(*@&$%*(&#&)%&(^#&*%)&*)#^()&^%(^)&*#%*(&^$^@(&*%$&@^$%*^(@)_)*%&^@^(&$)_@*&$^^#%$%^@$&)*(*(*%&^^*@&#$(*&)&(@*&^)&$%//
	//$%^&&^%*(*@&$%*(&#&)%&(^#&*%)&*)#^()&^%(^)&*#%*(&^$^@(&*%$&@^$%*^(@)_)*%&^@^(&$)_@*&$^^#%$%^@$&)*(*(*%&^^*@&#$(*&)&(@*&^)&$%//
	//$%^&&^%*(*@&$%*(&#&)%&(^#&*%)&*)#^()&^%(^)&*#%*(&^$^@(&*%$&@^$%*^(@)_)*%&^@^(&$)_@*&$^^#%$%^@$&)*(*(*%&^^*@&#$(*&)&(@*&^)&$%//
	
	//***************************************//
	//****************УЧИТЕЛЯ****************//

	public function teachers_list() {
		$this->loadModel('Teacher');	
		$teachers = $this->Teacher->find('all', array ('order' => array('name_1' => 'ASC', 'name_2' => 'ASC', 'name_3' => 'ASC')));
		
		$teachers_with_class_and_user = array();
		foreach ($teachers as $teacher) {
			$this->loadModel('MyClass');
			$teacher['Teacher']['class'] = $this->MyClass->find('first', array('conditions' => array('teacher_id' => $teacher['Teacher']['id'])));
			
			$this->loadModel('User');
			$teacher['Teacher']['user'] = $this->User->find('first', array('conditions' => array('id' => $teacher['Teacher']['user_id'])));
			
			array_push($teachers_with_class_and_user, $teacher);
		}
		
		$this->set('teachers_data', $teachers_with_class_and_user);
		$this->set('title_for_layout', 'Список учителей');
	}
	
	public function editTeacher($teacher_id = null) {
		if ($teacher_id != null) {
			
			$this->loadModel('Teacher');	
			$teacher = $this->Teacher->find('first', array ('conditions' => array('id' => $teacher_id)));
			
			if (!empty($this->request->data)) {

				//	Проверка на наличие пользователя с таким логином
				$this->loadModel('User');
				$user = $this->User->find('all', array ('conditions' => array('login' => $this->request->data['Admin']['login'])));

				if (count($user) > 1) {

					$this->set('error_text', 'Слишком много пользователей с таким логином! Проверьте данные учителей');			

				} else {

					print_r($this->request->data);
					print_r($user);

					if ((count($user) == 1) && ($user[0]['User']['id'] != $teacher['Teacher']['user_id'])) {

						$this->set('error_text', 'Пользователь с логином "'. $this->request->data['Admin']['login'] .'" уже существует');

					} else {

						//сохраняем данные учителя
						$this->loadModel('Teacher');
						
						$this->Teacher->id = $teacher_id;
						$data_teacher = array(
							'Teacher' => array(
								'name_1' => $this->request->data['Admin']['name_1'],
								'name_2' => $this->request->data['Admin']['name_2'],
								'name_3' => $this->request->data['Admin']['name_3']
						));
						$this->Teacher->save($data_teacher);
						
						//сохраняем данные пользователя
						$this->User->id = $teacher['Teacher']['user_id'];
						$data_user = array(
							'User' => array(
								'login' => $this->request->data['Admin']['login'],
								'pass' => $this->request->data['Admin']['pass']
						));
						$this->User->save($data_user);

						$this->redirect(array('controller' => 'admins', 'action' => 'teachers_list'));

					}

				}
			}			
			
			$this->loadModel('MyClass');
			$teacher['Teacher']['class'] = $this->MyClass->find('first', array('conditions' => array('teacher_id' => $teacher['Teacher']['id'])));
			
			$this->loadModel('User');
			$teacher['Teacher']['user'] = $this->User->find('first', array('conditions' => array('id' => $teacher['Teacher']['user_id'])));
			
			$this->set('teacher_data', $teacher);
			$this->set('formAction', 'editTeacher');
			$this->set('title_for_layout', 'Изменение данных учителя');
		} else {
			$this->redirect(array('controller' => 'admins', 'action' => 'teachers_list'));
		}
	}
	
	public function showTeacher($teacher_id = null) {
		if ($teacher_id != null) {
			
			$this->loadModel('Teacher');	
			$teacher = $this->Teacher->find('first', array ('conditions' => array('id' => $teacher_id)));			
			
			$this->loadModel('MyClass');
			$teacher['Teacher']['class'] = $this->MyClass->find('first', array('conditions' => array('teacher_id' => $teacher['Teacher']['id'])));
			
			$this->loadModel('User');
			$teacher['Teacher']['user'] = $this->User->find('first', array('conditions' => array('id' => $teacher['Teacher']['user_id'])));
			
			$this->set('teacher_data', $teacher);
			$this->set('formAction', 'editTeacher');
			$this->set('title_for_layout', 'Просмотр данных учителя');
		} else {
			$this->redirect(array('controller' => 'admins', 'action' => 'teachers_list'));
		}
	}
	
	public function deleteTeacher($teacher_id = null) {
		if ($teacher_id != null) {
			$this->loadModel('Teacher');
				
			$teacher = $this->Teacher->find('first', array ('conditions' => array('Teacher.id' => $teacher_id)));	
			
			//удаляем сперва учителя
			$this->deleteElementFromModel($teacher_id, 'Teacher');

			//Теперь необходимо удалить пользователя
			$this->deleteElementFromModel($teacher['Teacher']['user_id'], 'User');

			return $this->redirect(array('action' => 'teachers_list'));
		} else {
			$this->redirect(array('controller' => 'admins', 'action' => 'teachers_list'));
		}
	}
	
	public function addTeacher() {
		
		if (!empty($this->request->data)) {
			
			//	Проверка на наличие пользователя с таким логином
			$this->loadModel('User');
			$user = $this->User->find('first', array ('conditions' => array('User.login' => $this->request->data['Admin']['login'])));

			if (count($user) != 0) {

				$this->set('error_text', 'Пользователь с таким логином уже существует');

			} else {

				//создаем пользователя			
				$this->User->create();
				$data_user = array(
					'User' => array(
							'type' => '2',
							'login' => $this->request->data['Admin']['login'],
							'pass' => $this->request->data['Admin']['pass']
					)
				);
				if($this->User->save($data_user)){
					$userID = $this->User->id;
				} else {
					return;
				}
				
				//создаем учителя
				$this->loadModel('Teacher');
				$this->Teacher->create();
				$data_teacher = array(
					'Teacher' => array(
							'user_id' => $userID,
							'name_1' => $this->request->data['Admin']['name_1'],
							'name_2' => $this->request->data['Admin']['name_2'],
							'name_3' => $this->request->data['Admin']['name_3']
					)
				);
				$this->Teacher->save($data_teacher);

				$this->redirect(array('controller' => 'admins', 'action' => 'teachers_list'));
			}
		}
		
		$this->set('formAction', 'addTeacher');
		$this->set('title_for_layout', 'Добавление учителя в систему');
		
	}

	//****************УЧИТЕЛЯ****************//
	//***************************************//
	
	//$%^&&^%*(*@&$%*(&#&)%&(^#&*%)&*)#^()&^%(^)&*#%*(&^$^@(&*%$&@^$%*^(@)_)*%&^@^(&$)_@*&$^^#%$%^@$&)*(*(*%&^^*@&#$(*&)&(@*&^)&$%//
	//$%^&&^%*(*@&$%*(&#&)%&(^#&*%)&*)#^()&^%(^)&*#%*(&^$^@(&*%$&@^$%*^(@)_)*%&^@^(&$)_@*&$^^#%$%^@$&)*(*(*%&^^*@&#$(*&)&(@*&^)&$%//
	//$%^&&^%*(*@&$%*(&#&)%&(^#&*%)&*)#^()&^%(^)&*#%*(&^$^@(&*%$&@^$%*^(@)_)*%&^@^(&$)_@*&$^^#%$%^@$&)*(*(*%&^^*@&#$(*&)&(@*&^)&$%//
	//$%^&&^%*(*@&$%*(&#&)%&(^#&*%)&*)#^()&^%(^)&*#%*(&^$^@(&*%$&@^$%*^(@)_)*%&^@^(&$)_@*&$^^#%$%^@$&)*(*(*%&^^*@&#$(*&)&(@*&^)&$%//
	
	//***************************************//
	//****************УЧЕНИКИ****************//
	
	public function pupils_list() {
		$this->loadModel('Pupil');	
		
		$this->paginate = array(
			'limit' => 25,
	        'order' => array(
	            'MyClass.number' => 'ASC',
	            'MyClass.letter' => 'ASC',
	            'Pupil.name_1' => 'ASC',
	            'Pupil.name_2' => 'ASC',
	            'Pupil.name_3' => 'ASC'
	        )
	    );
		
		$pupils = $this->getPaginate('Pupil');
		
		$this->set('pupils', $pupils);
		$this->set('title_for_layout', 'Список учеников');
	}
	
	public function addPupil($class_id = null, $returnType = null) {
		
		$formAction = 'addPupil';	

		//проверяем, откуда мы пришли к этой страницы
		//чтобы туда же вернуться
		if ($class_id == null)
			//если из списка школы
			$returnAction = 'pupils_list';
		else {
			
			$formAction .= "/".$class_id;
			$formAction .= "/".$returnType;

			if ($returnType == 'list') {
				//если из списка классов
				$returnAction = 'class_list';
			} else if ($returnType == 'class') {
				//если из списка класса
				$returnAction = 'showClass/'.$class_id;
			} else {
				$returnAction = 'pupils_list';
			}
		}
		$this->set('returnAction', $returnAction);
		
		if (!empty($this->request->data)) {
			
			//создаем пользователя
			$this->loadModel('Pupil');
			$this->Pupil->create();

			if ($class_id == null) {
				$class_to_pupil = $this->request->data['Admin']['class_id'];
			} else {
				$class_to_pupil = $class_id;
			}

			$data_pupil = array(
				'Pupil' => array(
						'class_id' => $class_to_pupil,
						'name_1' => $this->request->data['Admin']['name_1'],
						'name_2' => $this->request->data['Admin']['name_2'],
						'name_3' => $this->request->data['Admin']['name_3'],
						'birth' => $this->request->data['Admin']['birth']
				)
			);
			if($this->Pupil->save($data_pupil)){
				$userID = $this->User->id;
			} else {
				return;
			}
			
			//возврат туда же, откуда пришли
			$this->redirect(array('controller' => 'admins', 'action' => $returnAction));
		}
		
		$this->loadModel('MyClass');
		$class = $this->MyClass->find('first', array('conditions' => array('MyClass.id' => $class_id)));
		$this->set('class', $class);

		$classes = $this->MyClass->find('all');
		$classes_with_teachers = array();
		foreach ($classes as $class) {
			$curr_class[$class['MyClass']['id']] = $class['MyClass']['number'].$class['MyClass']['letter'] . " (" .$class['Teacher']['name_1']." ".$class['Teacher']['name_2']." ".$class['Teacher']['name_3']. ")";
			$classes_with_teachers += $curr_class;
		}
		$this->set('classes_data', $classes_with_teachers);
		
		if ($class_id == null) {
			//если из списка школы
			$this->set('formAction', 'addPupil');
			$this->set('title_for_layout', 'Добавление ученика в школу');
		} else {
			//если из списка класса с ID=$class_id
			$this->set('formAction', $formAction);
			$this->set('title_for_layout', 'Добавление ученика в '.$class['MyClass']['number'].$class['MyClass']['letter'].' класс');
		}
	}
	
	public function deletePupil($pupil_id = null, $class_id = null, $returnType = null) {
		if ($pupil_id != null) {

			//проверяем, откуда мы пришли к этой страницы
			//чтобы туда же вернуться
			if ($class_id == null)
				//если из списка школы
				$returnAction = 'pupils_list';
			else {
				if ($returnType == 'list') {
					//если из списка классов
					$returnAction = 'class_list';
				} else if ($returnType == 'class') {
					//если из списка класса
					$returnAction = 'showClass/'.$class_id;
				} else {
					$returnAction = 'pupils_list';
				}
			}
						
			$this->deleteElementFromModel($pupil_id, 'Pupil');

			return $this->redirect(array('action' => $returnAction));
		} else {
			$this->redirect(array('controller' => 'admins', 'action' => 'pupils_list'));
		}
	}

	public function removePupilFromClass($pupil_id = null, $class_id = null) {
		if ($pupil_id != null) {
			$this->removePupilClass($pupil_id);
		}
		if ($class_id != null) {
			$this->redirect(array('controller' => 'admins', 'action' => 'showClass/'.$class_id));
		}	
	}
	
	public function editPupil($pupil_id = null, $class_id = null, $returnType = null) {
		if ($pupil_id != null) {

			$formAction = 'editPupil/'.$pupil_id;	

			//проверяем, откуда мы пришли к этой страницы
			//чтобы туда же вернуться
			if ($class_id == null)
				//если из списка школы
				$returnAction = 'pupils_list';
			else {
				
				$formAction .= "/".$class_id;
				$formAction .= "/".$returnType;

				if ($returnType == 'list') {
					//если из списка классов
					$returnAction = 'class_list';
				} else if ($returnType == 'class') {
					//если из списка класса
					$returnAction = 'showClass/'.$class_id;
				} else {
					$returnAction = 'pupils_list';
				}
			}
			$this->set('returnAction', $returnAction);

			if (!empty($this->request->data)) {
				//сохраняем данные ученика
				$this->loadModel('Pupil');
				
				$this->Pupil->id = $pupil_id;

				if ($this->request->data['Admin']['class_id'] == 'null')
					$class_to_pupil = null;
				else
					$class_to_pupil = $this->request->data['Admin']['class_id'];

				$data_pupil = array(
					'Pupil' => array(
						'class_id' => $class_to_pupil,
						'name_1' => $this->request->data['Admin']['name_1'],
						'name_2' => $this->request->data['Admin']['name_2'],
						'name_3' => $this->request->data['Admin']['name_3'],
						'birth' => $this->request->data['Admin']['birth']
				));
				$this->Pupil->save($data_pupil);

				$this->redirect(array('controller' => 'admins', 'action' => $returnAction));
			}
			
			
			$this->loadModel('Pupil');	
			$pupil = $this->Pupil->find('first', array ('conditions' => array('Pupil.id' => $pupil_id)));
			
			$this->loadModel('MyClass');
			$classes = $this->MyClass->find('all');
			$classes_with_teachers = array();
			foreach ($classes as $class) {
				$curr_class[$class['MyClass']['id']] = $class['MyClass']['number'].$class['MyClass']['letter'] . " (" .$class['Teacher']['name_1']." ".$class['Teacher']['name_2']." ".$class['Teacher']['name_3']. ")";
				$classes_with_teachers += $curr_class;
			}
			$this->set('classes_data', $classes_with_teachers);

			$this->set('pupil_data', $pupil);
			$this->set('formAction', $formAction);
			$this->set('title_for_layout', 'Изменение данных ученика');
		} else {
			$this->redirect(array('controller' => 'admins', 'action' => 'pupils_list'));
		}
	}

	public function showPupil($pupil_id = null, $class_id = null, $returnType = null) {
		if ($pupil_id != null) {

			//$formAction = 'showPupil/'.$pupil_id;	

			//проверяем, откуда мы пришли к этой страницы
			//чтобы туда же вернуться
			if ($class_id == null)
				//если из списка школы
				$returnAction = 'pupils_list';
			else {
				
				//$formAction .= "/".$class_id;
				//$formAction .= "/".$returnType;

				if ($returnType == 'list') {
					//если из списка классов
					$returnAction = 'class_list';
				} else if ($returnType == 'class') {
					//если из списка класса
					$returnAction = 'showClass/'.$class_id;
				} else {
					$returnAction = 'pupils_list';
				}
			}
			$this->set('returnAction', $returnAction);
			$this->set('title_for_layout', 'Просмотр данных ученика');		

			$this->loadModel('Semester');
			$last_8_semesters = $this->getLastSemester(8);

			//используем для задания массива отбора анкет
			$last_8_semesters_id = array();
			foreach ($last_8_semesters as $semester) {
				array_push($last_8_semesters_id, $semester['Semester']['id']);
			}

			$this->loadModel('Pupil');
			$pupil = $this->Pupil->find('first',
				array (
					'conditions' => array('Pupil.id' => $pupil_id),
					'contain' => array(
						'MyClass',
						'Anket' => array(
							'order' => array(
								'Anket.semester_id' => 'ASC'
							),
							'conditions' => array(
								'Anket.semester_id' => $last_8_semesters_id	//ищем анкеты за последние 8 четвертей
							),
							'Semester',
							'Mark' => array(
								'fields' => array('id','mark'),
								'Subject' => array(
									'fields' => array('id','name'),
									'Type' => array(
										'fields' => array('id','name'),
									)
								)
							),
							'Personal_data_anket' => array(
								'Personal_data_field',
								'Personal_data_answer'
							)
						)
					)
				)
			);
			// print_r($pupil);
			$this->set('pupil_data', $pupil);

			//----------------------------------
			$all_semester_marks = array();
			
			foreach ($pupil['Anket'] as $anket) {

				$curr_semester_marks = array();
				$curr_semester_marks['Anket'] = $anket;
				$curr_semester_marks['Semester'] = $anket['Semester'];
				$curr_semester_marks['Personal_data_anket'] = $anket['Personal_data_anket'];
				unset($curr_semester_marks['Anket']['Mark']);
				unset($curr_semester_marks['Anket']['Semester']);
				unset($curr_semester_marks['Anket']['Personal_data_anket']);

				foreach ($curr_semester_marks['Personal_data_anket'] as $key => $value) {
					unset($value['id']);
					unset($value['anket_id']);
					unset($value['personal_data_field_id']);
					unset($value['personal_data_answer_id']);
					$curr_semester_marks['Personal_data_anket'][$key] = $value;
				}

				$curr_semester_marks['Type'] = array();

				foreach ($anket['Mark'] as $mark) {
					if (!in_array($mark['Subject']['Type'], $curr_semester_marks['Type'])) {
						$new_type = $mark['Subject']['Type'];
						array_push($curr_semester_marks['Type'], $new_type);
					};
				};

				array_push($all_semester_marks, $curr_semester_marks);

			}

			// print_r($all_semester_marks);

			// echo '<br><br>';

			//	создание массивов для предметов внутри каждого типа
			foreach ($all_semester_marks as $key_sem => $value_sem) {	
				foreach ($value_sem['Type'] as $key_type => $value_type) {	
					$value_type['Subject'] = array();
					$value_sem['Type'][$key_type] = $value_type;
				};
				$all_semester_marks[$key_sem] = $value_sem;
			};

			//	создание массива различных предметов внутри соответствующих категорий
			foreach ($pupil['Anket'] as $anket) {
				foreach ($anket['Mark'] as $mark) {	

					foreach ($all_semester_marks as $key_sem => $value_sem) {

						foreach ($value_sem['Type'] as $key_type => $value_type) {	
							//добавление предмета в нужную категорию
							if ($mark['Subject']['Type']['id'] == $value_type['id']) {
								$new_subject = $mark['Subject'];
								if (!in_array($new_subject, $value_type['Subject'])) {
									array_push($value_type['Subject'], $new_subject);
									$value_sem['Type'][$key_type] = $value_type;
								}							
							}
						};

						$all_semester_marks[$key_sem] = $value_sem;

					};

				}
			}

			//	создание массивов для оценок внутри каждого предмета
			//	для назначения массива оценок даже для предметов, оценки которой нет в этом семестре
			foreach ($all_semester_marks as $key_sem => $value_sem) {	
				foreach ($value_sem['Type'] as $key_type => $value_type) {	
					
					foreach ($value_type['Subject'] as $key_sub => $value_sub) {
						$value_sub['Mark'] = array();
						$value_type['Subject'][$key_sub] = $value_sub;
					}

					$value_sem['Type'][$key_type] = $value_type;

				};
				$all_semester_marks[$key_sem] = $value_sem;
			};

			foreach ($pupil['Anket'] as $anket) {
				foreach ($anket['Mark'] as $mark) {

					foreach ($all_semester_marks as $key_sem => $value_sem) {	
						foreach ($value_sem['Type'] as $key_type => $value_type) {	
							
							foreach ($value_type['Subject'] as $key_sub => $value_sub) {
								
								if (($mark['Subject']['id'] == $value_sub['id']) && ($anket['semester_id'] == $value_sem['Semester']['id'])) {
									$value_sub['Mark'] = $mark;
									$value_type['Subject'][$key_sub] = $value_sub;
								}

							}

							$value_sem['Type'][$key_type] = $value_type;

						};
						$all_semester_marks[$key_sem] = $value_sem;
					};

				}
			}

			//	очистка лишних полей
			foreach ($all_semester_marks as $key_sem => $value_sem) {	
				foreach ($value_sem['Type'] as $key_type => $value_type) {	
					
					foreach ($value_type['Subject'] as $key_sub => $value_sub) {

						unset($value_sub['Type']);
						unset($value_sub['type_id']);
						unset($value_sub['Mark']['Subject']);
						unset($value_sub['Mark']['subject_id']);
						unset($value_sub['Mark']['anket_id']);

						$value_type['Subject'][$key_sub] = $value_sub;
					};
					$value_sem['Type'][$key_type] = $value_type;
				};
				$all_semester_marks[$key_sem] = $value_sem;
			};

			//	сортировка по возрастанию семестров
			$all_semester_marks = Set::sort($all_semester_marks, '{n}.Semester.date_start', 'asc');

			//	подсчет среднего балла оценок по каждой из категорий
			foreach ($all_semester_marks as $key_sem => $value_sem) {	
				foreach ($value_sem['Type'] as $key_type => $value_type) {	

					$type_sum = 0;
					$type_count = 0;
					
					foreach ($value_type['Subject'] as $key_sub => $value_sub) {

						//	не учитываем предметы, оценки которых нет в текущем сесестре
						if ($value_sub['Mark'] != null) {
							$type_sum += $value_sub['Mark']['mark'];
							$type_count++;
						}

					};

					$value_sem['Type'][$key_type]['type_mark'] = $type_sum / $type_count;
				};
				$all_semester_marks[$key_sem] = $value_sem;
			};

			$this->set('semesters_marks', $all_semester_marks);
			// print_r($all_semester_marks);
			//----------------------------------

		} else {
			$this->redirect(array('controller' => 'admins', 'action' => 'pupils_list'));
		}
	}

	//****************УЧЕНИКИ****************//
	//***************************************//
	
	//$%^&&^%*(*@&$%*(&#&)%&(^#&*%)&*)#^()&^%(^)&*#%*(&^$^@(&*%$&@^$%*^(@)_)*%&^@^(&$)_@*&$^^#%$%^@$&)*(*(*%&^^*@&#$(*&)&(@*&^)&$%//
	//$%^&&^%*(*@&$%*(&#&)%&(^#&*%)&*)#^()&^%(^)&*#%*(&^$^@(&*%$&@^$%*^(@)_)*%&^@^(&$)_@*&$^^#%$%^@$&)*(*(*%&^^*@&#$(*&)&(@*&^)&$%//
	//$%^&&^%*(*@&$%*(&#&)%&(^#&*%)&*)#^()&^%(^)&*#%*(&^$^@(&*%$&@^$%*^(@)_)*%&^@^(&$)_@*&$^^#%$%^@$&)*(*(*%&^^*@&#$(*&)&(@*&^)&$%//
	//$%^&&^%*(*@&$%*(&#&)%&(^#&*%)&*)#^()&^%(^)&*#%*(&^$^@(&*%$&@^$%*^(@)_)*%&^@^(&$)_@*&$^^#%$%^@$&)*(*(*%&^^*@&#$(*&)&(@*&^)&$%//
	
	//***************************************************//
	//****************КАТЕГОРИИ ПРЕДМЕТОВ****************//
	
	public function types_list() {
		
		$this->loadModel('Type');
		
		$types = $this->Type->find('all');		
		
		$this->set('types', $types);
		$this->set('title_for_layout', 'Список предметных категорий');
	}
	
	public function showType($type_id = null) {
		if ($type_id != null) {
			
			$this->loadModel('Type');
			$type = $this->Type->find('first', array('conditions' => array('id' => $type_id)));
			
			$this->set('title_for_layout', 'Просмотр предметов категории');

			$this->set('type_data', $type);
			
		} else {
			$this->redirect(array('controller' => 'admins', 'action' => 'subjects_list'));
		}
	}

	public function editType($type_id = null) {
		if ($type_id != null) {
			
			if (!empty($this->request->data)) {
				//сохраняем данные ученика
				$this->loadModel('Type');
				
				$this->Type->id = $type_id;
				$data_type = array(
					'Type' => array(
						'name' => $this->request->data['Admin']['name']
					)
				);
				$this->Type->save($data_type);

				$this->redirect(array('controller' => 'admins', 'action' => 'types_list'));
			}
			
			$this->loadModel('Type');	
			$type = $this->Type->find('first', array ('conditions' => array('Type.id' => $type_id)));
			
			$this->set('type_data', $type);
			$this->set('formAction', 'editType/'.$type_id);
			$this->set('returnAction', 'types_list');
			$this->set('title_for_layout', 'Изменение предметной категории');
		} else {
			$this->redirect(array('controller' => 'admins', 'action' => 'types_list'));
		}
	}

	public function addType() {
		
		if (!empty($this->request->data)) {
			//сохраняем данные ученика
			$this->loadModel('Type');
			
			$this->Type->create();
			$data_type = array(
				'Type' => array(
					'name' => $this->request->data['Admin']['name']
				)
			);
			$this->Type->save($data_type);

			$this->redirect(array('controller' => 'admins', 'action' => 'types_list'));
		}
		
		$this->set('formAction', 'addType');
		$this->set('returnAction', 'types_list');
		$this->set('title_for_layout', 'Создание предметной категории');	
	}

	public function deleteType($type_id = null) {
		if ($type_id != null) {
			$this->loadModel('Type');
			$type = $this->Type->find('first', array ('conditions' => array('Type.id' => $type_id)));
			$subjects = $type['Subject'];
			print_r($subjects);

			$this->deleteElementFromModel($type_id, 'Type');

			//удаляем также все связанные предметы
			if (!empty($subjects)) {
				foreach ($subjects as $subject) {
					$this->deleteElementFromModel($subject['id'], 'Subject');
				}
			}

			$this->redirect(array('action' => 'types_list'));
		} else {
			$this->redirect(array('controller' => 'admins', 'action' => 'types_list'));
		}
	}
	
	//****************КАТЕГОРИИ ПРЕДМЕТОВ****************//
	//***************************************************//

	//$%^&&^%*(*@&$%*(&#&)%&(^#&*%)&*)#^()&^%(^)&*#%*(&^$^@(&*%$&@^$%*^(@)_)*%&^@^(&$)_@*&$^^#%$%^@$&)*(*(*%&^^*@&#$(*&)&(@*&^)&$%//
	//$%^&&^%*(*@&$%*(&#&)%&(^#&*%)&*)#^()&^%(^)&*#%*(&^$^@(&*%$&@^$%*^(@)_)*%&^@^(&$)_@*&$^^#%$%^@$&)*(*(*%&^^*@&#$(*&)&(@*&^)&$%//
	//$%^&&^%*(*@&$%*(&#&)%&(^#&*%)&*)#^()&^%(^)&*#%*(&^$^@(&*%$&@^$%*^(@)_)*%&^@^(&$)_@*&$^^#%$%^@$&)*(*(*%&^^*@&#$(*&)&(@*&^)&$%//
	//$%^&&^%*(*@&$%*(&#&)%&(^#&*%)&*)#^()&^%(^)&*#%*(&^$^@(&*%$&@^$%*^(@)_)*%&^@^(&$)_@*&$^^#%$%^@$&)*(*(*%&^^*@&#$(*&)&(@*&^)&$%//
	
	//****************************************//
	//****************ПРЕДМЕТЫ****************//

	public function addSubject($type_id = null, $returnType = null) {
		if ($type_id != null) {

			$formAction = 'addSubject/'.$type_id;
			$formAction .= "/".$returnType;

			if ($returnType == 'list') {
				$returnAction = 'types_list';
			} else if ($returnType == 'type') {
				$returnAction = 'showType/'.$type_id;
			} else {
				$returnAction = 'types_list';
			}

			if (!empty($this->request->data)) {
				//сохраняем данные ученика
				$this->loadModel('Subject');
				
				$this->Subject->create();
				$data_subject = array(
					'Subject' => array(
						'name' => $this->request->data['Admin']['name'],
						'type_id' => $type_id,
						'start_study_year' => $this->request->data['Admin']['start_study_year'],
						'end_study_year' => $this->request->data['Admin']['end_study_year']
					)
				);
				$this->Subject->save($data_subject);

				$this->redirect(array('controller' => 'admins', 'action' => $returnAction));
			}

			$this->loadModel('Type');
			$type = $this->Type->find('first', array ('conditions' => array('Type.id' => $type_id)));
			$this->set('type_data', $type);
			
			$this->set('formAction', $formAction);
			$this->set('returnAction', $returnAction);
			$this->set('title_for_layout', 'Создание предмета');
		} else {
			$this->redirect(array('controller' => 'admins', 'action' => 'types_list'));
		}	
	}

	public function editSubject($subject_id = null, $type_id = null, $returnType = null) {
		if (!(($subject_id == null) || ($type_id == null))) {

			$formAction = 'editSubject/'.$subject_id."/".$type_id;
			$formAction .= "/".$returnType;

			if ($returnType == 'list') {
				$returnAction = 'types_list';
			} else if ($returnType == 'type') {
				$returnAction = 'showType/'.$type_id;
			} else {
				$returnAction = 'types_list';
			}

			if (!empty($this->request->data)) {
				//сохраняем данные ученика
				$this->loadModel('Subject');
				
				$this->Subject->id = $subject_id;
				$data_subject = array(
					'Subject' => array(
						'name' => $this->request->data['Admin']['name'],
						'start_study_year' => $this->request->data['Admin']['start_study_year'],
						'end_study_year' => $this->request->data['Admin']['end_study_year']
					)
				);
				$this->Subject->save($data_subject);

				$this->redirect(array('controller' => 'admins', 'action' => $returnAction));
			}

			$this->loadModel('Subject');
			$subject = $this->Subject->find('first', array ('conditions' => array('Subject.id' => $subject_id)));
			$this->set('subject_data', $subject);

			$this->loadModel('Type');
			$type = $this->Type->find('first', array ('conditions' => array('Type.id' => $type_id)));
			$this->set('type_data', $type);
			
			$this->set('formAction', $formAction);
			$this->set('returnAction', $returnAction);
			$this->set('title_for_layout', 'Изменение предмета');
		} else {
			$this->redirect(array('controller' => 'admins', 'action' => 'types_list'));
		}	
	}

	public function deleteSubject($subject_id = null, $type_id = null, $returnType = null) {
		if (!(($subject_id == null) || ($type_id == null))) {
			
			if ($returnType == 'list') {
				$returnAction = 'types_list';
			} else if ($returnType == 'type') {
				$returnAction = 'showType/'.$type_id;
			} else {
				$returnAction = 'types_list';
			}

			$this->deleteElementFromModel($subject_id, 'Subject');

			$this->redirect(array('controller' => 'admins', 'action' => $returnAction));
		} else {
			$this->redirect(array('controller' => 'admins', 'action' => 'types_list'));
		}
	}

	//****************ПРЕДМЕТЫ****************//
	//****************************************//
	
}

?>