<!--
<?php
	echo "Вот мое детище на 2 графа";
?>

<script type="text/javascript">

	var chartData1 = [
	{
		"name": "США",
		"visits": 4252,
		"buys": 321
	},
	{
		"name": "Китай",
		"visits": 1882,
		"buys": 3659
	}, {
		"name": "Япония",
		"visits": 1809,
		"buys": 956
	}, {
		"name": "Германия",
		"visits": 1322,
		"buys": 666
	}, {
		"name": "Британия",
		"visits": 1122,
		"buys": 123
	}, {
		"name": "Франция",
		"visits": 1114,
		"buys": 3645
	}, {
		"name": "India",
		"visits": 984,
		"buys": 2143
	}, {
		"name": "Spain",
		"visits": 711,
		"buys": 2034
	}, {
		"name": "Netherlands",
		"visits": 665,
		"buys": 1321
	}, {
		"name": "Russia",
		"visits": 580,
		"buys": 2321
	}, {
		"name": "South Korea",
		"visits": 443,
		"buys": 621
	}, {
		"name": "Canada",
		"visits": 441,
		"buys": 1500
	}, {
		"name": "Brazil",
		"visits": 395,
		"buys": 1029
	}, {
		"name": "Italy",
		"visits": 386,
		"buys": 1954
	}, {
		"name": "Australia",
		"visits": 384,
		"buys": 846
	}, {
		"name": "Taiwan",
		"visits": 338,
		"buys": 978
	}, {
		"name": "Poland",
		"visits": 328,
		"buys": 534
	}];

	var chartData2 = [{
		"name": "США",
		"visits": 4252,
		"buys": 321
	}, {
		"name": "Китай",
		"visits": 1882,
		"buys": 3659
	}, {
		"name": "Япония",
		"visits": 1809,
		"buys": 956
	}, {
		"name": "Германия",
		"visits": 1322,
		"buys": 666
	}, {
		"name": "Британия",
		"visits": 1122,
		"buys": 123
	}, {
		"name": "Франция",
		"visits": 1114,
		"buys": 3645
	}, {
		"name": "India",
		"visits": 984,
		"buys": 2143
	}, {
		"name": "Spain",
		"visits": 711,
		"buys": 2034
	}, {
		"name": "Netherlands",
		"visits": 665,
		"buys": 1321
	}, {
		"name": "Russia",
		"visits": 580,
		"buys": 2321
	}, {
		"name": "South Korea",
		"visits": 443,
		"buys": 621
	}, {
		"name": "Canada",
		"visits": 441,
		"buys": 1500
	}, {
		"name": "Brazil",
		"visits": 395,
		"buys": 1029
	}, {
		"name": "Italy",
		"visits": 386,
		"buys": 1954
	}, {
		"name": "Australia",
		"visits": 384,
		"buys": 846
	}, {
		"name": "Taiwan",
		"visits": 338,
		"buys": 978
	}, {
		"name": "Poland",
		"visits": 328,
		"buys": 534
	}];
	
	AmCharts.ready(function() {
		// SERIAL CHART
		chart1 = new AmCharts.AmSerialChart();
		chart1.dataProvider = chartData1;
		chart1.categoryField = "name";
		chart1.startDuration = 0.35;
		chart1.startEffect = ">";
		//chart1.angle = 30;
		//chart1.depth3D = 15;   

		// SERIAL CHART
		chart2 = new AmCharts.AmSerialChart();
		chart2.dataProvider = chartData2;
		chart2.categoryField = "name";
		chart2.startDuration = 0.35;
		chart2.startEffect = ">";
		//chart2.angle = 30;
		//chart2.depth3D = 15; 

		var yAxis1 = new AmCharts.ValueAxis();
		yAxis1.position = "left";

		var yAxis2 = new AmCharts.ValueAxis();
		yAxis2.position = "left";

		chart1.addValueAxis(yAxis1);
		chart2.addValueAxis(yAxis2);

		// AXES
		// category
		var categoryAxis = chart1.categoryAxis;
		categoryAxis.labelRotation = 45;
		categoryAxis.autoGridCount  = false;
		categoryAxis.gridCount = chartData1.length;                    
		categoryAxis.gridPosition = "start";

		// AXES
		// category
		categoryAxis = chart2.categoryAxis;
		categoryAxis.labelRotation = 45;
		categoryAxis.autoGridCount  = false;
		categoryAxis.gridCount = chartData2.length;                    
		categoryAxis.gridPosition = "start";

		// value
		// in case you don't want to change default settings of value axis,
		// you don't need to create it, as one value axis is created automatically.

		// GRAPH
		var graph = new AmCharts.AmGraph();
		graph.valueField = "visits";
		graph.balloonText = "[[category]]: посещения=<b>[[value]]</b>";
		graph.type = "line";
		//graph.fillAlphas = 0.8;
		graph.bullet = "round";
		graph.lineColor = "#001cc0";
		chart1.addGraph(graph);
		
		// GRAPH
		var graph2 = new AmCharts.AmGraph();
		graph2.valueField = "buys";
		graph2.balloonText = "[[category]]: покупки=<b>[[value]]</b>";
		graph2.type = "line";
		//graph2.fillAlphas = 0.8;
		graph2.bullet = "round";
		graph2.lineColor = "#ff1cc0";
		graph2.yAxis = yAxis2;
		chart2.addGraph(graph2);

		chart1.write("chart1div");
		chart2.write("chart2div");
	});

</script>


<div id="chart1div" style="width: 100%; height: 400px;"></div>

<div id="chart2div" style="width: 100%; height: 400px;"></div>

-->

<?php
	echo "Вот мое детище на 1 совмещенный граф";
?>

<button type="button" class="btn btn-primary button" onclick="javascript:openGraph(1)">1</button>
<button type="button" class="btn btn-primary button" onclick="javascript:openGraph(2)">2</button>
<button type="button" class="btn btn-primary button" onclick="javascript:openGraph(3)">2</button>

<div class="checkbox">

<input type="checkbox" checked name="data[chart][]" value="1" onchange="javascript:openGraph(1)" id="chartChackbox1"><label for="chartChackbox1">One</label>
<input type="checkbox" checked name="data[chart][]" value="1" onchange="javascript:openGraph(2)" id="chartChackbox1"><label for="chartChackbox1">Two</label>
<input type="checkbox" checked name="data[chart][]" value="1" onchange="javascript:openGraph(3)" id="chartChackbox1"><label for="chartChackbox1">Three</label>

</div>

<script type="text/javascript">

	var graph1;
	var graph2;

	function openGraph(number) {

		graphName = "graph"+number;
        chartName = "chart1";

        if (!window[graphName].hidden)
        	window[chartName].hideGraph(window[graphName]);
        else
        	window[chartName].showGraph(window[graphName]);
	}

	var chartData1 = [
	{
		"name": "США",
		"visits": 4252,
		"buys": 321,
		"other": 1000
	},
	{
		"name": "Китай",
		"visits": 1882,
		"buys": 3659,
		"other": 1500
	}, {
		"name": "Япония",
		"visits": 1809,
		"buys": 956,
		"other": 700
	}, {
		"name": "Германия",
		"visits": 1322,
		"buys": 666,
		"other": 900
	}, {
		"name": "Британия",
		"visits": 1122,
		"buys": 123,
		"other": 400
	}, {
		"name": "Франция",
		"visits": 1114,
		"buys": 3645,
		"other": 1800
	}, {
		"name": "India",
		"visits": 984,
		"buys": 2143,
		"other": 1500
	}, {
		"name": "Spain",
		"visits": 711,
		"buys": 2034
	}, {
		"name": "Netherlands",
		"visits": 665,
		"buys": 1321
	}, {
		"name": "Russia",
		"visits": 580,
		"buys": 2321
	}, {
		"name": "South Korea",
		"visits": 443,
		"buys": 621
	}, {
		"name": "Canada",
		"visits": 441,
		"buys": 1500
	}, {
		"name": "Brazil",
		"visits": 395,
		"buys": 1029
	}, {
		"name": "Italy",
		"visits": 386,
		"buys": 1954
	}, {
		"name": "Australia",
		"visits": 384,
		"buys": 846
	}, {
		"name": "Taiwan",
		"visits": 338,
		"buys": 978
	}, {
		"name": "Poland",
		"visits": 328,
		"buys": 534
	}];
	
	AmCharts.ready(function() {
		// SERIAL CHART
		chart1 = new AmCharts.AmSerialChart();
		chart1.dataProvider = chartData1;
		chart1.categoryField = "name";
		chart1.startDuration = 0.35;
		chart1.startEffect = ">";
		//chart1.angle = 30;
		//chart1.depth3D = 15;   

		var yAxis = new AmCharts.ValueAxis();
		yAxis.position = "left";
		yAxis.autoGridCount = false;
		yAxis.gridCount = 5;
		yAxis.minimum = 0;
		yAxis.maximum = 5000;

		// var yAxis1 = new AmCharts.ValueAxis();
		// yAxis1.position = "left";
		// yAxis1.autoGridCount = false;
		// yAxis1.gridCount = 5;
		// yAxis1.minimum = 0;
		// yAxis1.maximum = 5000;

		// var yAxis2 = new AmCharts.ValueAxis();
		// yAxis2.position = "left";
		// yAxis2.autoGridCount = false;
		// yAxis2.gridCount = 5;
		// yAxis2.minimum = 0;
		// yAxis2.maximum = 5000;

		// var yAxis3 = new AmCharts.ValueAxis();
		// yAxis3.position = "left";
		// yAxis3.autoGridCount = false;
		// yAxis3.gridCount = 5;
		// yAxis3.minimum = 0;
		// yAxis3.maximum = 5000;

		chart1.addValueAxis(yAxis);

		// AXES
		// category
		var categoryAxis = chart1.categoryAxis;
		categoryAxis.labelRotation = 45;
		categoryAxis.autoGridCount  = false;
		categoryAxis.gridCount = chartData1.length;                    
		categoryAxis.gridPosition = "start";

		// value
		// in case you don't want to change default settings of value axis,
		// you don't need to create it, as one value axis is created automatically.

		// GRAPH
		graph1 = new AmCharts.AmGraph();
		graph1.valueField = "visits";
		graph1.balloonText = "[[category]]: посещения=<b>[[value]]</b>";
		graph1.type = "line";
		//graph1.fillAlphas = 0.8;
		graph1.bullet = "round";
		graph1.lineColor = "#001cc0";
		//graph1.yAxis = yAxis1;
		chart1.addGraph(graph1);
		
		// GRAPH
		graph2 = new AmCharts.AmGraph();
		graph2.valueField = "buys";
		graph2.balloonText = "[[category]]: покупки=<b>[[value]]</b>";
		graph2.type = "line";
		//graph2.fillAlphas = 0.8;
		graph2.bullet = "round";
		graph2.lineColor = "#ff1cc0";
		//graph2.yAxis = yAxis2;
		chart1.addGraph(graph2);

		// GRAPH
		graph3 = new AmCharts.AmGraph();
		graph3.valueField = "other";
		graph3.balloonText = "[[category]]: прочее=<b>[[value]]</b>";
		graph3.type = "line";
		//graph3.fillAlphas = 0.8;
		graph3.bullet = "round";
		graph3.lineColor = "#000000";
		//graph3.yAxis = yAxis3;
		chart1.addGraph(graph3);

		chart1.write("chart1div");
	});

</script>


<div id="chart1div" style="width: 100%; height: 400px;"></div>

<div id="chart2div" style="width: 100%; height: 400px;"></div>