<h2>Список предметов категории</h2>
<h3><?php echo $type_data['Type']['name']; ?></h3>

<?php
	echo $this->Widgets->SubjectsTable($type_data['Subject'], "/type");

	echo $this->Html->link(
		'<i class="fa fa-plus"></i> Добавить предмет в категорию',													
		array('action' => 'addSubject/'.$type_data['Type']['id']),
		array(
			'escape' => false,
			'class' => 'button_add',
			'style' => 'margin-bottom: 20px'
	));
?>

<div class="contact-form">
	<?php
		echo $this->Html->link('Вернуться к списку категорий', array('action' => 'types_list'), array('class' => 'btn btn-primary button', 'div' => false,  'title' => "Вернуться к просмотру списка категорий предметов школы"));
	?>
</div>