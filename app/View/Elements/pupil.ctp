<h2><?php echo $title_for_layout; ?></h2>

<div class="contact-form">
<?php
	echo $this->Form->create('Admin', array('action' => $formAction, 'default' => true, 'id' => 'TeacherEditForm'));
	
	echo '<div class="inputs-left">';
	echo $this->Form->input('name_1', array(
		'type' => 'text',
		'class' => 'form-control',
		'label' => 'Фамилия',
		'value' => @$pupil_data['Pupil']['name_1'],
		'placeholder' => 'Введите фамилию'
	));
	
	echo $this->Form->input('name_2', array(
		'type' => 'text',
		'class' => 'form-control',
		'label' => 'Имя',
		'value' => @$pupil_data['Pupil']['name_2'],
		'placeholder' => 'Введите имя'
	));
	
	echo $this->Form->input('name_3', array(
		'type' => 'text',
		'class' => 'form-control',
		'label' => 'Отчество',
		'value' => @$pupil_data['Pupil']['name_3'],
		'placeholder' => 'Введите отчество'
	));

	$options = array(
		array(
            'name' => 'Без класса',
            'value' => 'null',
            'disabled' => false,
            'selected' => false
        )
    );

    if ($classes_data != null)
        	$options += $classes_data;

	echo $this->Form->input('class_id', array(
		'type' => 'select',
		'class' => 'form-control',
		'label' => 'Класс',
		'options' => @$options,
		'selected' => @$pupil_data['MyClass']['id']
	));
	
	echo '<div class="input date">';
		echo '<label>Дата рождения</label>';
		echo $this->Form->date('birth', array(
			'class' => 'form-control',
			'value' => @$pupil_data['Pupil']['birth']
		));
	echo '</div>';
		
	echo '</div>';
	
	echo $this->Form->submit('Сохранить', array('class' => 'btn btn-primary button', 'div' => false, 'style' => 'margin-right: 25px;'));
	echo $this->Html->link('Вернуться', array('action' => $returnAction), array('class' => 'btn btn-primary button', 'div' => false));
	echo $this->Form->end();
?>
</div>