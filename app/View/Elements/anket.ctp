﻿<h2><?php echo $title_for_layout; ?></h2>
<h3><?php echo $pupil['Pupil']['name_1'].' '.$pupil['Pupil']['name_2'].' '.$pupil['Pupil']['name_3'] ?></h3>

<div class="contact-form">
<?php

	echo $this->Form->create('Teacher', array('action' => $formAction.'/'.$pupil['Pupil']['id'].'/'.@$anket_id, 'default' => true, 'id' => 'AnketAddForm'));
	
	echo '<h3 style="text-align: left;">1) Предметные данные:</h3>';
	
		foreach ($subject_types as $type) {
			echo '<h4 style="text-align: left; padding-left: 30px;">'.$type['Type']['name'].'</h4>';
			echo '<div class="inputs-left">';
			foreach ($type['Type']['subjects'] as $subject) {
				echo $this->Form->input('subject-'.$subject['Subject']['id'], array(
					'type' => 'select',
					'class' => 'form-control',
					'label' => $subject['Subject']['name'],
					'options' => array( '1' => '1', '2' => '2', '3' => '3', '4' => '4', '5' => '5'),
					'default' => '5',
					'selected' => @$subject['Subject']['mark']
				));
			}
			echo '</div>';
		}
		
	echo '<h3 style="text-align: left;">2) Комплексное тестирование:</h3>';
	
	echo '<div class="inputs-left">';
	echo $this->Form->input('mark_complex', array(
		'type' => 'select',
		'class' => 'form-control',
		'label' => 'Оценка',
		'options' => array( '1' => '1', '2' => '2', '3' => '3', '4' => '4', '5' => '5'),
		'default' => '5',
		'selected' => @$anket['Anket']['mark_complex']
	));
	echo '</div>';
	
	echo '<h3 style="text-align: left;">3) Личностные достижения:</h3>';
	
	$count = 1;
	echo '<div class="inputs-left">';

	if (!$editMode) {
		foreach ($personal_data as $data) {
			echo '<label style="text-align: left; width: 100%;">'.$count . ") " . $data['Personal_data_field']['name'].'</label>';
			echo $this->Form->input('personal-'.$data['Personal_data_field']['id'], array(
				'type' => 'select',
				'class' => 'form-control',
				'label' => false,
				'options' => $personal_data_answers,
				'default' => '4',
				'selected' => @$subject['Subject']['mark'],
				'style' => 'margin-left: 30%;'
			));
			$count++;
		}
	} else {
		foreach ($anket['Personal_data_anket'] as $data) {
			echo '<label style="text-align: left; width: 100%;">'.$count . ") " . $data['Personal_data_field']['name'].'</label>';
			echo $this->Form->input('personal-'.$data['Personal_data_field']['id'], array(
				'type' => 'select',
				'class' => 'form-control',
				'label' => false,
				'options' => $personal_data_answers,
				'default' => '4',
				'selected' => $data['Personal_data_answer']['id'],
				'style' => 'margin-left: 30%;'
			));
			$count++;
		}
	}
	echo '</div>';
	unset($count);

	echo $this->Form->submit('Сохранить', array('class' => 'btn btn-primary button', 'div' => false, 'style' => 'margin-right: 25px;'));
	
	echo $this->Html->link('Вернуться', array('action' => 'cabinet'), array('class' => 'btn btn-primary button', 'div' => false));

	echo $this->Form->end();
	echo $this->Js->writeBuffer();
	
?>
</div>