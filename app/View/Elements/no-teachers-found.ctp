<h2>Нет свободных учителей в школе</h2>
<h4>Освободите или наймите хотя бы одного учителя</h4>
<p>У каждого класса должен быть назначен учитель. Если свободных учителей нет, то нет смысла создавать и класс, так как возможности заполнять анкеты учеников этого класса не будет, а значит теряется первоначальный замысел системы.</p>
<div class="contact-form">
	<?php
		echo $this->Html->link('Добавить учителя', array('action' => 'addTeacher'), array('class' => 'btn btn-primary button', 'div' => false, 'style' => 'margin-right: 25px;'));
		echo $this->Html->link('Вернуться к списку классов', array('action' => 'class_list'), array('class' => 'btn btn-primary button', 'div' => false));
	?>
</div>