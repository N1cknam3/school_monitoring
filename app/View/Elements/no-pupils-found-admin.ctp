<h2>Нет учеников в <?php echo @$class['number'].@$class['letter']; ?> классе</h2>
<p>Каждый ученик относится к какому-то классу. Если есть ученики без определенного класса, либо у класса просто нет учеников, то на экран выводится соответствующее сообщение.</p>

<div class="contact-form">
	<?php
		echo $this->Html->link('Добавить ученика в класс', array('action' => 'addPupil/'.$class['id']), array('class' => 'btn btn-primary button', 'div' => false, 'style' => 'margin-right: 25px;', 'title' => "Добавить ученика в текущий класс"));
		echo $this->Html->link('Вернуться к списку классов', array('action' => 'class_list'), array('class' => 'btn btn-primary button', 'div' => false,  'title' => "Вернуться к просмотру списка классов школы"));
	?>
</div>