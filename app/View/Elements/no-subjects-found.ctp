<h2>Нет предметов в категории</h2>
<h3><?php echo @$type_data['Type']['name']; ?></h3>
<p>Каждый предмет относится к какой-то категории предметов. Если у категории нет предметов, то на экран выводится соответствующее сообщение. Добавьте предметы в эту категорию. Также проверьте базу данных на наличие предметов, не относящихся ни к одной из категорий.</p>

<div class="contact-form">
	<?php
		echo $this->Html->link('Добавить предмет в категорию', array('action' => 'addSubject/'.$type_data['Type']['id']), array('class' => 'btn btn-primary button', 'div' => false, 'style' => 'margin-right: 25px;', 'title' => "Добавить предмет в эту категорию"));
		echo $this->Html->link('Вернуться к списку категорий', array('action' => 'types_list'), array('class' => 'btn btn-primary button', 'div' => false,  'title' => "Вернуться к просмотру списка категорий предметов школы"));
	?>
</div>