<h2><?php echo $title_for_layout; ?></h2>

<div class="contact-form">
<?php
	echo $this->Form->create('Admin', array('action' => $formAction.'/'.@$teacher_data['Teacher']['id'], 'default' => true, 'id' => 'TeacherEditForm'));
	
	echo '<div class="inputs-left">';
	echo $this->Form->input('name_1', array(
		'type' => 'text',
		'class' => 'form-control',
		'label' => 'Фамилия',
		'value' => @$teacher_data['Teacher']['name_1'],
		'placeholder' => 'Введите фамилию'
	));
	
	echo $this->Form->input('name_2', array(
		'type' => 'text',
		'class' => 'form-control',
		'label' => 'Имя',
		'value' => @$teacher_data['Teacher']['name_2'],
		'placeholder' => 'Введите имя'
	));
	
	echo $this->Form->input('name_3', array(
		'type' => 'text',
		'class' => 'form-control',
		'label' => 'Отчество',
		'value' => @$teacher_data['Teacher']['name_3'],
		'placeholder' => 'Введите отчество'
	));
	
	echo $this->Form->input('login', array(
		'type' => 'text',
		'class' => 'form-control',
		'label' => 'Логин',
		'value' => @$teacher_data['Teacher']['user']['User']['login'],
		'placeholder' => 'Введите логин пользователя'
	));
	
	echo $this->Form->input('pass', array(
		'type' => 'text',
		'class' => 'form-control',
		'label' => 'Пароль',
		'value' => @$teacher_data['Teacher']['user']['User']['pass'],
		'placeholder' => 'Введите пароль пользователя'
	));

	echo '<div class="status-message" style="margin-left: 30%;">'.@$error_text.'</div>';
		
	echo '</div>';
	
	echo $this->Form->submit('Сохранить', array('class' => 'btn btn-primary button', 'div' => false, 'style' => 'margin-right: 25px;'));
	echo $this->Html->link('Вернуться к списку учителей', array('action' => 'teachers_list'), array('class' => 'btn btn-primary button', 'div' => false));
	echo $this->Form->end();
?>
</div>