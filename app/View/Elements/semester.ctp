<h2><?php echo $title_for_layout; ?></h2>

<div class="contact-form">
<?php
	echo $this->Form->create('Admin', array('action' => $formAction.'/'.@$semester['Semester']['id'], 'default' => true, 'id' => 'SemesterAddForm'));
	
	echo '<div class="inputs-left">';
	echo '<label>Начало</label>';
	echo $this->Form->date('date_start', array(
			'class' => 'form-control date',
			'label' => 'Начало',
			'value' => @$semester['Semester']['date_start']
		));
	
	echo "<br>";
	echo '<label>Конец</label>';
	echo $this->Form->date('date_end', array(
			'class' => 'form-control date',
			'label' => 'Конец',
			'value' => @$semester['Semester']['date_end']
		));
		
	echo '</div>';
	
	echo $this->Form->submit('Сохранить', array('class' => 'btn btn-primary button', 'div' => false, 'style' => 'margin-right: 25px;'));
	echo $this->Html->link('Вернуться', array('action' => 'semester_list'), array('class' => 'btn btn-primary button', 'div' => false));
	echo $this->Form->end();
?>
</div>