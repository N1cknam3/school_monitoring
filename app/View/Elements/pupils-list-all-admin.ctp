<h2>Список учащихся школы</h2>

<table class="table table-striped table-hover pupil-list" style="margin-bottom: 0px;">
	<thead>
		<tr>
			<th>#</th>
			<th><?php echo $this->Paginator->sort('name_1', 'Фамилия'); ?></th>
			<th><?php echo $this->Paginator->sort('name_2', 'Имя'); ?></th>
			<th><?php echo $this->Paginator->sort('name_3', 'Отчество'); ?></th>
			<th><?php echo $this->Paginator->sort('birth', 'Дата рождения'); ?></th>
			<th><?php echo $this->Paginator->sort('MyClass.number', 'Класс'); ?></th>
			<th>Кол-во анкет</th>
			<th class="button-area col-md-2">Действия</th>
		</tr>
		</thead>
	<tbody>
	
		<?php
			$count = 0;
			foreach($pupils as $pupil) {
				$count++;
				echo $this->Widgets->PupilsSchoolTableRow($pupil, $count);
			}
		?>
		
	</tbody>
</table>

<?php
	echo $this->Html->link(
		'<i class="fa fa-plus"></i> Добавить ученика',													
		array('action' => 'addPupil'),
		array(
			'escape' => false,
			'class' => 'button_add'
	));
	echo '<div class="paging" style="text-align: right;">';
		echo $this->Paginator->prev('< ' . __('Назад'), array(), null, array('class' => 'bprev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__('Вперед') . ' >', array(), null, array('class' => 'next disabled'));
		echo '<div class="link-body" style="float: right;">';
			echo $this->Paginator->link('10', array('limit' => 10), array('class' => 'first link', 'title' => 'Выводить по 10 записей'));
			echo $this->Paginator->link('25', array('limit' => 25), array('class' => 'middle link', 'title' => 'Выводить по 25 записей'));
			echo $this->Paginator->link('50', array('limit' => 50), array('class' => 'last link', 'title' => 'Выводить по 50 записей'));
		echo '</div>';
	echo '</div>';
	echo '<div class="paging-footer">';
		echo $this->Paginator->counter(array(
			'format' => __('Страница {:page} из {:pages}, записей показано {:current} из {:count}, начиная с {:start} по {:end}')
		));
	echo '</div>';
?>

<p>*В этой таблице можно сортировать записи, кликнув по соответствующему заглавию области описания (выделено цветом)</p>