<h2><?php echo $title_for_layout; ?></h2>

<div class="contact-form">
<?php
	echo $this->Form->create('Admin', array('action' => $formAction.'/'.@$class_data['MyClass']['id'], 'default' => true, 'id' => 'ClassEditForm'));
	
	echo '<div class="inputs-left">';
	echo $this->Form->input('teacher_id', array(
		'type' => 'select',
		'class' => 'form-control',
		'label' => 'Классный руководитель',
		'options' => @$teachers_data,
		'selected' => @$current_teacher,
		'style' => 'width: 40% !important; margin-right: 0px;'
	));
	
	echo $this->Form->input('number', array(
		'type' => 'select',
		'class' => 'form-control',
		'label' => 'Год обучения',
		'options' => array('1' => '1', '2' => '2', '3' => '3', '4' => '4', '5' => '5', '6' => '6', '7' => '7', '8' => '8', '9' => '9', '10' => '10', '11' => '11'),
		'default' => '1',
		'selected' => @$class_data['MyClass']['number']
	));

	echo $this->Form->input('letter', array(
		'type' => 'select',
		'class' => 'form-control',
		'label' => 'Буква класса',
		'options' => array(
			'А' => 'А',
			'Б' => 'Б',
			'В' => 'В',
			'Г' => 'Г',
			'Д' => 'Д',
			'Е' => 'Е',
			'Ж' => 'Ж',
			'З' => 'З',
			'И' => 'И',
			'К' => 'К',
			'Л' => 'Л'
		),
		'default' => 'А',
		'selected' => @$class_data['MyClass']['letter']
	));
		
	echo '</div>';
	
	echo $this->Form->submit('Сохранить', array('class' => 'btn btn-primary button', 'div' => false, 'style' => 'margin-right: 25px;'));
	echo $this->Html->link('Вернуться', array('action' => 'class_list'), array('class' => 'btn btn-primary button', 'div' => false));
	echo $this->Form->end();
?>
</div>