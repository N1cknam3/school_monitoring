<h2><?php echo $title_for_layout; ?></h2>

<h3>Категория: <?php echo $type_data['Type']['name']; ?></h3>

<div class="contact-form">
<?php
	echo $this->Form->create('Admin', array('action' => $formAction, 'default' => true, 'id' => 'SubjectEditForm'));
	
	echo '<div class="inputs-left">';
	echo $this->Form->input('name', array(
		'type' => 'text',
		'class' => 'form-control',
		'label' => 'Название',
		'value' => @$subject_data['Subject']['name'],
		'placeholder' => 'Введите название предмета'
	));
	
	echo $this->Form->input('start_study_year', array(
		'type' => 'select',
		'class' => 'form-control',
		'label' => 'Начало обучения',
		'options' => array('1' => '1', '2' => '2', '3' => '3', '4' => '4', '5' => '5', '6' => '6', '7' => '7', '8' => '8', '9' => '9', '10' => '10', '11' => '11'),
		'default' => '1',
		'selected' => @$subject_data['Subject']['start_study_year']
	));

	echo $this->Form->input('end_study_year', array(
		'type' => 'select',
		'class' => 'form-control',
		'label' => 'Конец обучения',
		'options' => array('1' => '1', '2' => '2', '3' => '3', '4' => '4', '5' => '5', '6' => '6', '7' => '7', '8' => '8', '9' => '9', '10' => '10', '11' => '11'),
		'default' => '11',
		'selected' => @$subject_data['Subject']['end_study_year']
	));
		
	echo '</div>';
	
	echo $this->Form->submit('Сохранить', array('class' => 'btn btn-primary button', 'div' => false, 'style' => 'margin-right: 25px;'));
	echo $this->Html->link('Вернуться', array('action' => $returnAction), array('class' => 'btn btn-primary button', 'div' => false));
	echo $this->Form->end();
?>
</div>