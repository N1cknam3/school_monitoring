<h2>Список учащихся <?php echo $class['number'].' '.$class['letter'].' класса'?></h2>
<h3>Классный руководитель: <?php echo $teacher['name_1'].' '.$teacher['name_2'].' '.$teacher['name_3'] ?></h3>

<table class="table table-striped table-hover pupil-list">
	<thead>
		<tr>
			<th>#</th>
			<th>Фамилия</th>
			<th>Имя</th>
			<th>Отчество</th>
			<th class="button-area">Действия</th>
		</tr>
		</thead>
	<tbody>
	
		<?php
			$count = 0;
			foreach($pupils as $pupil) {
				$count++;
				if (empty($pupil['Pupil']['anket'])) {
					//Вывод ученика с кнопкой "Добавление" и красным цветом
					//так как у него нет ни одной анкеты
					echo $this->Widgets->PupilsTableRowDanger($pupil, $count);
				} else {
					if ($pupil['Pupil']['anket']['state'] == 'new') {
						//Вывод ученика с кнопкой "Редактировать" и "Удалить" и зеленым цветом
						//так как у него есть активная анкета
						echo $this->Widgets->PupilsTableRowSuccess($pupil, $count);
					} else {
						//Вывод ученика с кнопкой "Добавление" и "Редактировать" и желтым цветом
						//так как у него есть только устаревшая анкета
						echo $this->Widgets->PupilsTableRowWarning($pupil, $count);
					}
				}
			}
		?>
		
	</tbody>
</table>