<h2><?php echo $title_for_layout; ?></h2>

<div class="contact-form">
<?php
	echo $this->Form->create('Admin', array('action' => $formAction, 'default' => true, 'id' => 'TypeEditForm'));
	
	echo '<div class="inputs-left">';
	echo $this->Form->input('name', array(
		'type' => 'text',
		'class' => 'form-control',
		'label' => 'Фамилия',
		'value' => @$type_data['Type']['name'],
		'placeholder' => 'Введите название'
	));
	echo '</div>';
	
	echo $this->Form->submit('Сохранить', array('class' => 'btn btn-primary button', 'div' => false, 'style' => 'margin-right: 25px;'));
	echo $this->Html->link('Вернуться', array('action' => $returnAction), array('class' => 'btn btn-primary button', 'div' => false));
	echo $this->Form->end();
?>
</div>