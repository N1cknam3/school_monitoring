<h2>Список учащихся <?php echo $class['MyClass']['number'].' '.$class['MyClass']['letter'].' класса'?></h2>
<h3>Классный руководитель: <?php echo $class['Teacher']['name_1'].' '.$class['Teacher']['name_2'].' '.$class['Teacher']['name_3'] ?></h3>

<?php
	echo $this->Widgets->PupilsTable($class['Pupil'], "/class");

	echo $this->Html->link(
		'<i class="fa fa-plus"></i> Добавить ученика в класс',													
		array('action' => 'addPupil/'.@$class['id']),
		array(
			'escape' => false,
			'class' => 'button_add',
			'style' => 'margin-bottom: 20px;'
	));
?>

<div class="contact-form">
	<?php
		echo $this->Html->link('Вернуться к списку классов', array('action' => 'class_list'), array('class' => 'btn btn-primary button', 'div' => false));
	?>
</div>