<h2>Нет учеников в школе</h2>
<p>Если нет учеников в базе данных школы, то на экран выводится соответствующее сообщение.</p>

<div class="contact-form">
	<?php
		echo $this->Html->link('Добавить ученика', array('action' => 'addPupil'), array('class' => 'btn btn-primary button', 'div' => false, 'style' => 'margin-right: 25px;', 'title' => "Добавить ученика"));
		echo $this->Html->link('Вернуться в кабинет', array('action' => 'cabinet'), array('class' => 'btn btn-primary button', 'div' => false,  'title' => "Вернуться в личный кабинет завуча"));
	?>
</div>