
<!DOCTYPE html>
<html lang="en">

<!--HEAD - ONLOAD-->
<head>
	
<!--Адаптация разрешения под разрешение мобильного устройства-->	
<meta name="viewport" content="width=device-width, initial-scale=1.0">
	
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<?php echo $this->Html->charset(); ?>

<title><?php echo $this->fetch('title'); ?></title>

<?php
	echo $this->Html->meta('icon');

	echo $this->Html->css('all-vendors');
	echo $this->Html->css('style-admin');
	echo $this->Html->css('responsive');

	echo $this->Html->css('font-awesome');
	
	echo $this->Html->script('jquery-1.7.1.min');
	echo $this->Html->script('jquery.form');
	echo $this->Html->script('functions');	

	echo $this->fetch('meta');
	echo $this->fetch('css');
	echo $this->fetch('script');
?>
</head>


<body class="admin">
<?php echo $this->Session->flash(); ?>

	<div class="content">
		
	<div id="myModal" class="modal fade login in" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="false" style="display: none;">
	    <div class="modal-dialog">
		    
		    <?php echo $this->Form->create('Faculty', array('action' => 'ajax_speciality_add', 'default' => false, 'id' => 'SpecialityForm')); ?>
		    	<div class="modal-content">
			    	
			        <div class="modal-header">
			        	<button type="button" class="close" data-dismiss="modal" aria-hidden="true" onclick="javascript:closepass()">x</button>
				        <h4 class="modal-title">
				        	Форма изменения/добавления</h4>
			        </div>
			        
			        <div class="modal-body">
						
						<?php				
						
					?>
					
					<div id="formAddEditContent"></div>
					
					<div id="status" class="status"></div>
						
			        </div>
			        
			        <div class="modal-footer">
						<button type="button" class="btn btn-default" data-dismiss="modal" aria-hidden="true" onclick="javascript:closepass()">
						Закрыть
						</button>
						<!-- <input class="btn btn-primary" type="submit" value="Восстановить" id="" name="submit"> -->
						<?php 
							echo $this->Form->submit('Сохранить', array('class'=>'btn btn-primary', 'div'=>false));
							echo $this->Js->writeBuffer();
		          		?>
			        </div>
			        
		    	</div>
		    <!-- </form> -->
		    <?php echo $this->Form->end(); ?>
	    </div>    
	</div>
	
	<div class="sidebar">
		<div class="sidebar-dropdown">
			<a id="btn-main" href="javascript:dropDownMenu()" class="">Расписание</a>
			
		</div>
		<ul id="nav" class="main-nav" style="">
			<!--
			<div class="nav_logo" style="text-align: center;">
				<h1 style="color: white;"></h1>
			</div>
			-->
			
			<li class="nav_alternative">    
		    	<ul class="nav_alternative_controls" tabindex="-1" data-reactid=".1.0.1.0">
					<li class="url-link " data-link="#"><i class="icon-file-text"></i></li>
					<li class="url-link " data-link="#"><i class="icon-list-ul"></i> </li>
					<li class="url-link " data-link="#"><i class="icon-time"></i></li>
					<li class="url-link " data-link="#"><i class="icon-sitemap"></i></li>
					<li class="url-link " data-link="#"><i class="icon-wrench"></i></li>
		    	</ul>
			</li>
			
			<li>
				<?php					
					echo $this->Html->link(
						'<i class="icon-home"></i> Главная',
						array(
							'controller' => 'faculties',
							'action' => 'admin'
						),
						array(
							'escape' => false,
							'class' => @$mainClass
						)
					);
				?>
			</li>
			
			<li class="has_sub"> <a id="dropdownButton1" class="<?php echo @$dropdownButton1Class; ?>" href="javascript:dropdownButton()">  
			    <i class="icon-list"></i> Специальности<span class="pull-right"><i id="dropdownIcon1" class="<?php echo @$dropdownIcon1Class; ?>" style="font-size:12px"></i></span></a>
			    <ul id="dropdownList11" style="<?php echo @$dropdownList1Style; ?>">
				    
				    <li>
					<?php
					echo $this->Html->link(
						' Бакалавр',
						array(
							'controller' => 'faculties',
							'action' => 'specialities/0'
						),
						array(
							'escape' => false,
							'class' => @$specialityClass
						)
					);
					?>
				    </li>
				    
				    <li>
					<?php
					echo $this->Html->link(
						' Магистр',
						array(
							'controller' => 'faculties',
							'action' => 'specialities/1'
						),
						array(
							'escape' => false,
							'class' => @$specialityClass
						)
					);
					?>
				    </li>
				    
				    <li>
					<?php
					echo $this->Html->link(
						' Специалитет',
						array(
							'controller' => 'faculties',
							'action' => 'specialities/2'
						),
						array(
							'escape' => false,
							'class' => @$specialityClass
						)
					);
					?>
				    </li>
				    
			    </ul>
			  </li>
			
			<li>
				<a href="#" class="">
					<i class="icon-folder-open-alt"></i> Архив
				</a>
			</li>
			
		</ul>
	</div>
	
	<div class="mainbar">
		<?php echo $this->element('admin-header'); ?>
		<div class="matter">
	
			<?php echo $this->element('admin-title'); ?>
			
			<div class="container">
				<?php echo $this->fetch('content'); ?>
			</div>
			
		</div>
	</div>
</div>

<div class="clearfix"></div>

<?php echo $this->element('footer'); ?>
	

</body>
</html>
