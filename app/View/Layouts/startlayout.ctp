﻿<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.View.Layouts
 * @since         CakePHP(tm) v 0.10.0.1076
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

$title = 'Система мониторинга';
//$developerInfo = 'Powered by: Jungle knights, 2015 (C)';

?>

<!DOCTYPE html>
<html>
<head>
	<?php echo $this->Html->charset(); ?>
	<title>
		<?php echo $title ?>:
		<?php echo $this->fetch('title'); ?>
	</title>
	<?php
		echo $this->Html->meta('icon');

		//стили темы
		echo $this->Html->css('style');
		echo $this->Html->css('animate');
		echo $this->Html->css('bootstrap.min');
		echo $this->Html->css('font-awesome.min');
		echo $this->Html->css('normalize.min');
		echo $this->Html->css('templatemo_misc');
		echo $this->Html->css('templatemo_style');
		
		//функции для графиков
		echo $this->Html->script('amchart/amcharts/amcharts.js');
		echo $this->Html->script('amchart/amcharts/serial.js');	
		
		//мои функции
		echo $this->Html->script('jquery-1.7.1.min');
		echo $this->Html->script('jquery.form');		
		echo $this->Html->script('functions');
		
		//функции темы

		echo $this->Html->script('jquery.easing-1.3.js');
		echo $this->Html->script('bootstrap.js');
		echo $this->Html->script('plugins.js');
		echo $this->Html->script('main.js');

		echo $this->fetch('meta');
		echo $this->fetch('css');
		echo $this->fetch('script');
	?>
</head>



<body>
    <!--[if lt IE 7]>
    <p class="chromeframe">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> or <a href="http://www.google.com/chromeframe/?redirect=true">activate Google Chrome Frame</a> to improve your experience.</p>
    <![endif]-->
    
    <div class="bg-overlay"></div>

    <div class="container-fluid">
        <div class="row">
            
            <div class="col-md-4">
                <div class="sidebar-menu">
                    
                    <div class="logo-wrapper">
                        <h1 class="logo">Система мониторинга</h1>
                    </div> <!-- /.logo-wrapper -->
                    
                    <div class="menu-wrapper">
                        <ul class="menu">
							<li><a class="show-1" href="#">Вход</a></li>
                            <li><a class="show-2" href="#">Помощь</a></li>
                        </ul> <!-- /.menu -->
                        <a href="#" class="toggle-menu"><i class="fa fa-bars"></i></a>
                    </div> <!-- /.menu-wrapper -->

                </div> <!-- /.sidebar-menu -->
            </div> <!-- /.col-md-4 -->

            <div class="col-md-8 col-sm-12">
                
                <div id="menu-container">
				
					<div id="menu-1" class="contact content animated fadeInDown" style="display: block;">
                        <div class="row">
                            
                            <div class="col-md-12">
                                <div class="contact-form">
                                    <div class="row">
                                    	<?php echo $this->Form->create('Login', array('action' => 'ajax_login', 'default' => false, 'id' => 'LoginForm')); ?>
										
											<div class="col-md-12 row">
												<fieldset class="col-md-6">
													<div class="input-group">
														<span class="input-group-addon">@</span>
														<input id="login" type="text" class="form-control" name="login" placeholder="Логин">
													</div>
												</fieldset>
											</div>
											<div class="col-md-12 row">
												<fieldset class="col-md-6">
													<div class="input-group">
														<span class="input-group-addon"><i class="fa fa-key"></i></span>
														<input id="pass" type="password" class="form-control" name="pass" placeholder="Пароль">
													</div>
												</fieldset>
											</div>
																				
                                            <fieldset class="col-md-3">
                                                <!-- <input type="submit" value="Войти" id="submit" class="button">	-->
												<?php
													echo $this->Form->submit('Войти', array('class'=>'btn btn-primary button', 'div'=>false));								
												?>
                                            </fieldset>											
											<div class="col-md-9">
												<div id="status" class="status-message" style="display: none;"></div>
												<?php echo $this->fetch('content'); ?>
											</div>									
                                        <?php
											echo $this->Form->end();
											echo $this->Js->writeBuffer();
										?>
										
                                    </div> <!-- /.row -->
                                </div> <!-- /.contact-form -->
                            </div> <!-- /.col-md-12 -->
                        </div> <!-- /.row -->
                    </div> <!-- /.contact -->

                    <div id="menu-2" class="about content">
                        <div class="row">
                            <ul class="tabs">
                                <li class="col-md-6 col-sm-6">
                                    <a href="#tab1" class="icon-item">
                                        <i class="fa fa-cog"></i>
                                    </a> <!-- /.icon-item -->
                                </li>
                                <li class="col-md-6 col-sm-6">
                                    <a href="#tab2" class="icon-item">
                                        <i class="fa fa-question"></i>
                                    </a> <!-- /.icon-item -->
                                </li>
                            </ul> <!-- /.tabs -->
                            <div class="col-md-12 col-sm-12">
                                <div class="toggle-content text-center" id="tab1">
                                    <h3>Как пользоваться программой</h3>
                                    <p>Circle is one of free HTML5 website templates from templatemo. You may tell your friends about <strong><span class="blue">template</span><span class="green">mo</span></strong> website. Feel free to download, modify and use this template for your websites. You can easily change icons by <a rel="nofollow" href="http://fontawesome.info/font-awesome-icon-world-map/">Font Awesome</a>. Example: <strong>&lt;i class=&quot;fa fa-camera&quot;&gt;&lt;/i&gt;</strong> 
                                    <br><br>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Libero, repellat, aspernatur nihil quasi commodi laboriosam cumque est minus minima sit dicta adipisci possimus magnam. Sit, repudiandae, ut, error, voluptates aspernatur inventore quo earum reiciendis dolorum amet perspiciatis adipisci itaque voluptatum iste laboriosam sapiente hic autem blanditiis doloribus nihil.</p>
                                </div>

                                <div class="toggle-content text-center" id="tab2">
                                    <h3>О программе</h3>
                                    <p>Donec quis orci nisl. Integer euismod lacus nec risus sollicitudin molestie vel semper turpis. In varius imperdiet enim quis iaculis. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Mauris ac mauris aliquam magna molestie posuere in id elit. Integer semper metus felis, fringilla congue elit commodo a. Donec eget rutrum libero.
                                    <br><br>Nunc dui elit, vulputate vitae nunc sed, accumsan condimentum nisl. Vestibulum a dui lectus. Vivamus in justo hendrerit est cursus semper sed id nibh. Donec ut dictum lorem, eu molestie nisi. Quisque vulputate quis leo lobortis fermentum. Ut sit amet consectetur dui, vitae porttitor lectus.</p>
                                </div>
                            </div> <!-- /.col-md-12 -->
                        </div> <!-- /.row -->
                        
                    </div> <!-- /.about -->

                </div> <!-- /#menu-container -->

            </div> <!-- /.col-md-8 -->

        </div> <!-- /.row -->
    </div> <!-- /.container-fluid -->
    
    <div class="container-fluid">   
        <div class="row">
            <div class="col-md-12 footer">
                <p id="footer-text">Copyright &copy; 2015 | <a href="#">Шечкова Анна</a></p>
            </div><!-- /.footer --> 
        </div>
    </div> <!-- /.container-fluid -->

	<script src="<?php echo $this->webroot; ?>js/vendor/jquery-1.10.1.min.js"></script>
    <script>window.jQuery || document.write('<script src="<?php echo $this->webroot; ?>js/vendor/jquery-1.10.1.min.js"><\/script>')</script>
    <script src="<?php echo $this->webroot; ?>js/jquery.easing-1.3.js"></script>
    <script src="<?php echo $this->webroot; ?>js/bootstrap.js"></script>
    <script src="<?php echo $this->webroot; ?>js/plugins.js"></script>
    <script src="<?php echo $this->webroot; ?>js/main.js"></script>
	
</body>