﻿<h2><?php echo $title_for_layout; ?></h2>
								
<table class="table table-hover pupil-list" style="margin-bottom: 0px;">
	<thead>
		<tr>
			<th>#</th>
			<th>Класс</th>
			<th>Руководитель</th>
			<th class="button-area col-md-3">Действия</th>
		</tr>
		</thead>
	<tbody>
	
		<?php
			$count = 0;
			foreach($class_data as $class) {
				$count++;
				$toggleName = "pupils-of-class-".$count;
				
				if (!empty($class['Pupil'])) {
					echo $this->Widgets->ClassTableRow($class, $count, "'#".$toggleName."'", 'success');
					echo '<tr id="'.$toggleName.'" style="display: none;">';
						echo '<td colspan="4" style="padding-right: 0px; padding-top: 0px; padding-bottom: 0px; padding-left: 20px; background-color: rgb(253, 221, 148);">';
							echo '<div id="'.$toggleName.'-div" style="display: none; ">';
								echo $this->Widgets->PupilsTable($class['Pupil'], "/list");
							echo '</div>';
						echo '</td>';
					echo '</tr>';
				} else {
					echo $this->Widgets->ClassTableRow($class, $count, "'#".$toggleName."'");
				}
			}
		?>
		
	</tbody>
</table>

<?php
	echo $this->Html->link(
		'<i class="fa fa-plus"></i> Сформировать новый класс',													
		array('action' => 'addClass'),
		array(
			'escape' => false,
			'class' => 'button_add'
	));
?>

<p>*В таблице все строки, подсвеченные зеленым, раскрываются при клике по области описания</p>