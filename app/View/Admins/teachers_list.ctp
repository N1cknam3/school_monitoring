﻿<h2><?php echo $title_for_layout; ?></h2>
								
<table class="table table-striped table-hover pupil-list" style="margin-bottom: 0px;">
	<thead>
		<tr>
			<th>#</th>
			<th>ФИО</th>
			<th>Класс</th>
			<th class="button-area col-md-2">Действия</th>
		</tr>
		</thead>
	<tbody>
	
		<?php
			$count = 0;
			foreach($teachers_data as $teacher) {
				$count++;
				echo $this->Widgets->TeacherTableRow($teacher, $count);
			}
		?>
		
	</tbody>
</table>

<?php
	echo $this->Html->link(
		'<i class="fa fa-plus"></i> Добавить нового учителя',													
		array('action' => 'addTeacher'),
		array(
			'escape' => false,
			'class' => 'button_add'
	));
?>