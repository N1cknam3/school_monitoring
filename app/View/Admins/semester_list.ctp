﻿<h2>Список четвертей обучения</h2>	
								
<table class="table table-striped table-hover pupil-list" style="margin-bottom: 0px;">
	<thead>
		<tr>
			<th>#</th>
			<th>Начало</th>
			<th>Конец</th>
			<th class="button-area">Действия</th>
		</tr>
		</thead>
	<tbody>
	
		<?php
			$count = 0;
			foreach($semester_data as $semester) {
				$count++;
				if ($semester['Semester']['id']==$last_semester['Semester']['id']){
					//Вывод ячейки зеленого цвета, чтобы показать текущий семестр

					if ($semesterIsActive) {
						echo $this->Widgets->SemesterTableRow($semester, $count, 'success');
					} else {
						echo $this->Widgets->SemesterTableRow($semester, $count, 'danger');
					}
				} else {
					//Вывод обычной ячейки
					echo $this->Widgets->SemesterTableRow($semester, $count);
				}
			}
		?>
		
	</tbody>
</table>
							
<?php
	echo $this->Html->link(
		'<i class="fa fa-plus"></i> Начать новый семестр',													
		array('action' => 'addSemester'),
		array(
			'escape' => false,
			'class' => 'button_add'
	));
	echo '<div class="paging" style="text-align: right;">';
		echo $this->Paginator->prev('< ' . __('Назад'), array(), null, array('class' => 'bprev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__('Вперед') . ' >', array(), null, array('class' => 'next disabled'));
		echo '<div class="link-body" style="float: right;">';
			echo $this->Paginator->link('10', array('limit' => 10), array('class' => 'first link', 'title' => 'Выводить по 10 записей'));
			echo $this->Paginator->link('25', array('limit' => 25), array('class' => 'middle link', 'title' => 'Выводить по 25 записей'));
			echo $this->Paginator->link('50', array('limit' => 50), array('class' => 'last link', 'title' => 'Выводить по 50 записей'));
		echo '</div>';
	echo '</div>';
	echo '<div class="paging-footer">';
		echo $this->Paginator->counter(array(
			'format' => __('Страница {:page} из {:pages}, записей показано {:current} из {:count}, начиная с {:start} по {:end}')
		));
	echo '</div>';
?>

<p>
	*Цветом отмечена текущая активная четверть:
	<br>
	<span style="color: green">Зеленый</span> - четверть уже началась (учителя могут работать с анкетами)
	<br>
	<span style="color: red">Красный</span> - четверть ещё не началась (работа с анкетами невозможна)
<p>

**Помните, что если текущая дата в кабинете учителя не будет находиться в пределах действия активной четверти (включительно), то учитель не сможет заполнять анкеты учеников
<br>
Либо задавайте дату начала/конца четверти с запасом, либо создавайте четверть своевременно.
<br>
Вы также можете в любой момент изменить пределы действия четверти
</p>