<h2><?php echo $title_for_layout." ".$pupil_data['MyClass']['number'].$pupil_data['MyClass']['letter']." класса"; ?></h2>

<h3><?php echo $pupil_data['Pupil']['name_1']." ".$pupil_data['Pupil']['name_2']." ".$pupil_data['Pupil']['name_3']; ?></h3>

<?php

	//------------------------------
	//ВЫВОД МАССИВА ДАННЫХ ПРЕДМЕТОВ
	$count = 1;
	echo '<script type="text/javascript">';
		
		foreach ($types as $type) {
			foreach ($type['Subject'] as $subject) {

				$chartDataName = "chartDataName".$count;
				echo 'var '.$chartDataName.' = "'.$subject['name'].'";';	//глобальная переменная имени графа - название предмета
				echo $this->Widgets->NewChartDataMark($subject['Mark'], $last_8_semesters, $count);
				
				$count++;
			};
		}
		$count--;

	echo '</script>';
	//ВЫВОД МАССИВА ДАННЫХ ПРЕДМЕТОВ
	//------------------------------
?>

<script type="text/javascript">
	
	<?php
		//Вывод глобальных переменных для дальнейшего управления графами
		for ($i=1; $i <= $count; $i++) { 
			echo 'var graph'.$i.';';
		}
	?>

	AmCharts.ready(function() {

		<?php
			//вывод настроек графов
			for ($i=1; $i <= $count; $i++) { 
				echo $this->Widgets->NewChartSettings($i, "#001cc0");
			}
		?>

	});

</script>

<!-- ВИДИМАЯ ОБЛАСТЬ -->
<div class="contact-form">

	<div id="chart'.$i.'div" style="width: 100%; height: 200px;">

	<?php
	for ($i=1; $i <= $count; $i++) { 
		if ($i != 1)
			echo '<div id="chart'.$i.'div" class="chartDiv" style="width: 100%; height: 200px; display: none;"></div>';
		else
			echo '<div id="chart'.$i.'div" class="chartDiv" style="width: 100%; height: 200px;"></div>';
	}
	?>

	</div>

<?php

	// for ($i=1; $i <= $count; $i++) { 
	// 	$toggleName = "chart".$i."div";
		
	// 	echo $this->Form->button($i, array('type' => 'button', 'class' => 'btn btn-primary button', 'div' => false, 'onclick' => 'javascript:openGraph('."'#".$toggleName."')"));
	// }	

	echo '<div class="inputs-left" style="margin-bottom: 50px;">';

		// $options = array();
		// $new_count = 1;	//необходимо, чтобы нумерация начиналась с нуля, чтобы правильно сформировать id элементов
		// foreach ($types as $type) {
		// 	foreach ($type['Subject'] as $subject) {

		// 		$options[$new_count] = $subject['name'];
		// 		$new_count++;
		// 	};
		// }
		// unset($new_count);		

		// echo $this->Form->input('chart',array(
		// 	'label' => false,
		// 	'type' => 'select',
		// 	'multiple' => 'checkbox',
		// 	'options' => $options,
		// 	'id' => 'chartCheckbox',
		// 	'selected' => 1	//по умолчанию выделяем первый
		// ));

		$new_count = 1;	//необходимо, чтобы нумерация начиналась с нуля, чтобы правильно сформировать id элементов
		foreach ($types as $type) {

			echo '<div class="typeName">'.$type['name'].'</div>';

			foreach ($type['Subject'] as $subject) {				

				if ($new_count != 1) {
					echo '<div id="radioDiv'.$new_count.'" class="radioDiv"><input type="radio" name="group1" id="chartRadio'.$new_count.'" value="'.$new_count.'">'.$subject['name'].'</div>';
				} else {
					echo '<div id="radioDiv'.$new_count.'" class="radioDiv"><input type="radio" name="group1" id="chartRadio'.$new_count.'" value="'.$new_count.'" checked="checked">'.$subject['name'].'</div>';
				};

				$new_count++;
			};

		}
		unset($new_count);

		// for ($i = 1; $i <= $count; $i++) { 
		// 	if ($i != 1) {
		// 		echo '<div id="radioDiv'.$i.'" class="radioDiv"><input type="radio" name="group1" id="chartRadio'.$i.'" value="'.$i.'">'.$options[$i].'</div>';
		// 	} else {
		// 		echo '<div id="radioDiv'.$i.'" class="radioDiv"><input type="radio" name="group1" id="chartRadio'.$i.'" value="'.$i.'" checked="checked">'.$options[$i].'</div>';
		// 	}
		// }

		// echo $this->Form->radio('Выбор предмета', $options, array(
		// 	'label' => false,
		// 	'type' => 'radio',
		// 	'id' => 'chartRadio',
		// 	'value' => 1	//по умолчанию выделяем первый
		// ));
		
		// echo '<label>ФИО:</label>';
		// echo @$teacher_data['Teacher']['name_1'] ." ". @$teacher_data['Teacher']['name_2'] ." ". @$teacher_data['Teacher']['name_3'];
		
		// echo '<br>';
		// echo '<label>Логин:</label>';
		// echo @$teacher_data['Teacher']['user']['User']['login'];
		
		// echo '<br>';
		// echo '<label>Пароль:</label>';
		// echo @$teacher_data['Teacher']['user']['User']['pass'];

		// echo '<br>';

	echo '</div>';

	echo $this->Html->link('Вернуться', array('action' => $returnAction), array('class' => 'btn btn-primary button', 'div' => false));
?>

<p>*Приведены данные за последние 2 года (8 учебных периодов)</p>

</div>