<h2><?php echo $title_for_layout." ".$pupil_data['MyClass']['number'].$pupil_data['MyClass']['letter']." класса"; ?></h2>

<h3><?php echo $pupil_data['Pupil']['name_1']." ".$pupil_data['Pupil']['name_2']." ".$pupil_data['Pupil']['name_3']; ?></h3>

<?php
	// print_r($pupil_data);
	// print_r($semesters_marks);

	$new_array = $this->Widgets->MakeChartPHPArray($semesters_marks);

	// print_r($new_array['Personal_data']);

	//задаем для каждого пункта строго фиксированные номера и цвета графов
	$chartData_subjects = $this->Widgets->MakeSubjectChartNumberPHPArray($new_array['Subjects_data'], 1);
	$chartData_complex = $this->Widgets->MakeChartNumberPHPArray($new_array['Complex_data'], sizeof($chartData_subjects) + 1);
	$chartData_personal = $this->Widgets->MakeChartNumberPHPArray($new_array['Personal_data'], $chartData_complex['mark_complex']['number'] + 1);

	// echo "<br><br>";
	// print_r($new_array['Subjects_data']);
	// echo "<br><br>";
	// print_r($chartData_subjects);
	// echo "<br><br>";
	// print_r($chartData_complex);
	// echo "<br><br>";
	// print_r($chartData_personal);

	//------------------------------
	//ВЫВОД МАССИВА ДАННЫХ ПРЕДМЕТОВ
	echo '<script type="text/javascript">';

		echo $this->Widgets->MakeChartDataFromSubject($new_array['Subjects_data'], 1);

		echo $this->Widgets->MakeChartDataFrom($new_array['Complex_data'], 2);

		echo $this->Widgets->MakeChartDataFrom($new_array['Personal_data'], 3);

	echo '</script>';
	//ВЫВОД МАССИВА ДАННЫХ ПРЕДМЕТОВ
	//------------------------------
?>

<script type="text/javascript">
	
	<?php
		//Вывод глобальных переменных для дальнейшего управления графами
		foreach ($chartData_subjects as $key => $value) {
			echo 'var graph'.$value['number'].';';
		}
		foreach ($chartData_complex as $key => $value) {
			echo 'var graph'.$value['number'].';';
		}
		foreach ($chartData_personal as $key => $value) {
			echo 'var graph'.$value['number'].';';
		}
	?>

	AmCharts.ready(function() {

		<?php
			//вывод настроек графов
			echo $this->Widgets->GetChartSettings($chartData_subjects, 1);
			echo $this->Widgets->GetChartSettings($chartData_complex, 2);
			echo $this->Widgets->GetChartSettings($chartData_personal, 3);
		?>

	});

</script>

<!-- ВИДИМАЯ ОБЛАСТЬ -->
<div class="contact-form">

<?php

	echo '<div class="inputs-left" style="margin-bottom: 50px;">';
		echo '<h3>1) Предметные данные:</h3>';
		echo '<div id="chart1div" class="chartDiv" style="width: 100%; height: 200px;"></div>';

		foreach ($chartData_subjects as $key => $value) {		
			if ($value['type'] == "subject") {
				if ($value['number'] != 1) {
					echo '<div id="checkDiv'.$value['number'].'" class="checkDiv subject">						
						<input type="checkbox" id="chartCheck'.$value['number'].'" chart="chart1" graph="graph'.$value['number'].'"/>
						<div class="checkDiv color" style="display: inline-block; width: 10px; height: 10px; border: 6px solid '.$value['color'].'"></div>
						<label class="subjectName">'.$key.'</label>
					</div>';
				} else {
					echo '<div id="checkDiv'.$value['number'].'" class="checkDiv subject active">
						<input type="checkbox" checked id="chartCheck'.$value['number'].'" chart="chart1" graph="graph'.$value['number'].'"/>
						<div class="checkDiv color" style="display: inline-block; width: 10px; height: 10px; border: 6px solid '.$value['color'].'"></div>
						<label class="subjectName">'.$key.'</label>
					</div>';
				};
			} else if ($value['type'] == "type") {
				if ($value['number'] != 1) {
					echo '<div id="checkDiv'.$value['number'].'" class="checkDiv type">
						<input type="checkbox" id="chartCheck'.$value['number'].'" chart="chart1" graph="graph'.$value['number'].'"/>
						<div class="checkDiv color" style="display: inline-block; width: 10px; height: 10px; border: 6px solid '.$value['color'].'"></div>
						<label class="typeName">'.$key.'</label>
					</div>';
				} else {
					echo '<div id="checkDiv'.$value['number'].'" class="checkDiv type active">
						<input type="checkbox" checked id="chartCheck'.$value['number'].'" chart="chart1" graph="graph'.$value['number'].'"/>
						<div class="checkDiv color" style="display: inline-block; width: 10px; height: 10px; border: 6px solid '.$value['color'].'"></div>
						<label class="typeName">'.$key.'</label>
					</div>';
				};
			}

		}
	echo '</div>';

	echo '<div class="inputs-left" style="margin-bottom: 50px;">';
		echo '<h3>2) Комплексное тестирование:</h3>';
		echo '<div id="chart2div" class="chartDiv" style="width: 100%; height: 200px;"></div>';
	echo '</div>';

	echo '<div class="inputs-left" style="margin-bottom: 50px;">';
		echo '<h3>3) Личные достижения:</h3>';
		echo '<div id="chart3div" class="chartDiv" style="width: 100%; height: 200px;"></div>';

		$count = 1;
		foreach ($chartData_personal as $key => $value) {		

			if ($count != 1) {
				echo '<div id="checkDiv'.$value['number'].'" class="checkDiv" style="margin-bottom: 10px;">						
					<input type="checkbox" id="chartCheck'.$value['number'].'" chart="chart3" graph="graph'.$value['number'].'"/>
					<div class="checkDiv color" style="display: inline-block; width: 10px; height: 10px; border: 6px solid '.$value['color'].'"></div>
					<label class="subjectName" style="display: inline;">'.$key.'</label>
				</div>';
			} else {
				echo '<div id="checkDiv'.$value['number'].'" class="checkDiv active" style="margin-bottom: 10px;">
					<input type="checkbox" checked id="chartCheck'.$value['number'].'" chart="chart3" graph="graph'.$value['number'].'"/>
					<div class="checkDiv color" style="display: inline-block; width: 10px; height: 10px; border: 6px solid '.$value['color'].'"></div>
					<label class="subjectName" style="display: inline;">'.$key.'</label>
				</div>';
			};
			$count++;
		}
	echo '</div>';

	unset($count);

	echo $this->Html->link('Вернуться', array('action' => $returnAction), array('class' => 'btn btn-primary button', 'div' => false));
?>

<p>*Приведены данные за последние 2 года (8 учебных периодов)</p>

</div>