﻿<?php 
	//форма входа
	$Formdata = $this->Js->get('#LoginForm')->serializeForm(
                                                array(
                                                'isForm' => true,
                                                'inline' => true)
                                            );
 
    // Submit the serialize data on submit click
 
    $this->Js->get('#LoginForm')->event(
          'submit',
          $this->Js->request(
            array('controller' => 'logins', 'action' => 'ajax_login'),
            array(
                    'update' => '#status', // element to update
                                             // after form submission
                    'complete' => "ajax_login()",
                    'data' => $Formdata,
                    'async' => true,
                    'dataExpression'=>true,
                    'method' => 'POST'
                )
            )
        );
    echo $this->Js->writeBuffer();
?>