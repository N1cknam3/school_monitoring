<?php
class WidgetsHelper extends AppHelper {
	
	public $helpers = array('Html', 'Form');
	
	public function PupilsTableRowDanger($pupil, $count){		
		$answer = '';
		
		$answer .= '<tr class="danger">';
			$answer .= '<th scope="row">'. @$count .'</th>';
			$answer .= '<td>'. $pupil['Pupil']['name_1'] .'</td>';
			$answer .= '<td>'. $pupil['Pupil']['name_2'] .'</td>';
			$answer .= '<td>'. $pupil['Pupil']['name_3'] .'</td>';
			$answer .= '<td class="button-area">';
				$answer .= $this->Html->link(__(''), array('action' => 'addAnket/'.$pupil['Pupil']['id']), array('escape' => false, 'title' => "Создать новую анкету", 'class' => 'button col-md-12 fa fa-plus'));
			$answer .= '</td>';
		$answer .= '</tr>';
		
		return $answer;
	}
	
	public function PupilsTableRowWarning($pupil, $count){		
		$answer = '';
		
		$answer .= '<tr class="warning">';
			$answer .= '<th scope="row">'. @$count .'</th>';
			$answer .= '<td>'. $pupil['Pupil']['name_1'] .'</td>';
			$answer .= '<td>'. $pupil['Pupil']['name_2'] .'</td>';
			$answer .= '<td>'. $pupil['Pupil']['name_3'] .'</td>';
			$answer .= '<td class="button-area">';				
				$answer .= $this->Html->link(__(''), array('action' => 'addAnket/'.$pupil['Pupil']['id']), array('escape' => false, 'title' => "Создать новую анкету", 'class' => 'button col-md-6 fa fa-plus'));
				$answer .= $this->Html->link(__(''), array('action' => 'editAnket/'.$pupil['Pupil']['id'].'/'.$pupil['Pupil']['anket']['id']), array('escape' => false, 'title' => "Изменить старую анкету", 'class' => 'button col-md-6 fa fa-pencil'));
			$answer .= '</td>';
		$answer .= '</tr>';
		
		return $answer;
	}
	
	public function PupilsTableRowSuccess($pupil, $count){	
		$answer = '';
		
		$answer .= '<tr class="success">';
			$answer .= '<th scope="row">'. @$count .'</th>';
			$answer .= '<td>'. $pupil['Pupil']['name_1'] .'</td>';
			$answer .= '<td>'. $pupil['Pupil']['name_2'] .'</td>';
			$answer .= '<td>'. $pupil['Pupil']['name_3'] .'</td>';
			
			$name = $pupil['Pupil']['name_1'] .' '. $pupil['Pupil']['name_2'] .' '. $pupil['Pupil']['name_3'];
			
			$answer .= '<td class="button-area">';				
				$answer .= $this->Html->link(__(''), array('action' => 'editAnket/'.$pupil['Pupil']['id'].'/'.$pupil['Pupil']['anket']['id']), array('escape' => false, 'title' => "Изменить активную анкету", 'class' => 'button col-md-6 fa fa-pencil'));
				$answer .= $this->Form->postLink(__(''), array('action' => 'deleteAnket/'.$pupil['Pupil']['anket']['id']), array('confirm' => 'Вы действительно хотите удалить анкету учащегося: '.$name."?", 'escape' => false, 'title' => "Удалить активную анкету", 'class' => 'button col-md-6 fa fa-trash-o'));
			$answer .= '</td>';
		$answer .= '</tr>';
		
		return $answer;
	}

	public function PupilsTableRow($pupil, $count, $returnAction = null){	
		$answer = '';
		
		$answer .= '<tr>';
			$answer .= '<th scope="row">'. @$count .'</th>';
			$answer .= '<td>'. $pupil['name_1'] .'</td>';
			$answer .= '<td>'. $pupil['name_2'] .'</td>';
			$answer .= '<td>'. $pupil['name_3'] .'</td>';

			if (!empty($pupil['birth'])) {
				$date = strftime("%d %B %Y", strtotime($pupil['birth']));
				if ($date[0] == '0')
					$date = substr($date, 1);
				$answer .= '<td>'. $date .'</td>';
			} else
				$answer .= '<td>Нет значения</td>';

			$anket_count = count($pupil['Anket']);

			$answer .= '<td>'. $anket_count .'</td>';
			
			$name = $pupil['name_1'] .' '. $pupil['name_2'] .' '. $pupil['name_3'];
			
			$answer .= '<td class="button-area">';	

				if ($anket_count > 0) {

					$answer .= $this->Html->link(
						__(''),
						array('action' => 'showPupil/'.$pupil['id']."/".$pupil['class_id'].$returnAction),
						array('escape' => false, 'title' => "Просмотреть данные ученика", 'class' => 'button col-md-4 fa fa-search')
					);
					$answer .= $this->Html->link(
						__(''),
						array('action' => 'editPupil/'.$pupil['id']."/".$pupil['class_id'].$returnAction),
						array('escape' => false, 'title' => "Изменить данные ученика", 'class' => 'button col-md-4 fa fa-pencil')
					);
					$answer .= $this->Form->postLink(
						__(''),
						array('action' => 'deletePupil/'.$pupil['id']."/".$pupil['class_id'].$returnAction),
						array(
							'confirm' => 'Вы действительно хотите удалить ученика '.$name." из школы?",
							'escape' => false,
							'title' => "Удалить ученика из школы",
							'class' => 'button col-md-4 fa fa-times'
						)
					);
				} else {
					$answer .= $this->Html->link(
						__(''),
						'#',
						array('onclick' => "javascript:alert('Нет анкет. Просмотр данных невозможен.');", 'escape' => false, 'title' => "Просмотр данных невозможен", 'class' => 'button col-md-4 fa fa-exclamation-triangle')
					);
					$answer .= $this->Html->link(
						__(''),
						array('action' => 'editPupil/'.$pupil['id']."/".$pupil['class_id'].$returnAction),
						array('escape' => false, 'title' => "Изменить данные ученика", 'class' => 'button col-md-4 fa fa-pencil')
					);
					$answer .= $this->Form->postLink(
						__(''),
						array('action' => 'deletePupil/'.$pupil['id']."/".$pupil['class_id'].$returnAction),
						array(
							'confirm' => 'Вы действительно хотите удалить ученика '.$name." из школы?",
							'escape' => false,
							'title' => "Удалить ученика из школы",
							'class' => 'button col-md-4 fa fa-times'
						)
					);
				}

				//$answer .= $this->Html->link(__(''), array('action' => 'showPupil/'.$pupil['id']), array('escape' => false, 'title' => "Просмотреть данные ученика", 'class' => 'button col-md-6 fa fa-search'));
				//$answer .= $this->Form->postLink(__(''), array('action' => 'removePupilFromClass/'.$pupil['id']."/".$pupil['class_id']), array('confirm' => 'Вы действительно хотите удалить ученика '.$name." из класса?", 'escape' => false, 'title' => "Удалить ученика из класса", 'class' => 'button col-md-6 fa fa-times'));
			$answer .= '</td>';
		$answer .= '</tr>';
		
		return $answer;
	}

	public function TableRowWorkTable($workTable) {
		$answer = '';

		$answer .= '<tr>';
			$answer .= '<td>';
			$answer .= $this->Html->link(
						$workTable['name'],
						array('action' => 'workTable/'.$workTable['id']),
						array('escape' => false, 'title' => $workTable['name'])
					);
			$answer .= '<td>';
		$answer .= '</tr>';

		return $answer;
	}
	
	public function PupilsSchoolTableRow($pupil, $count){	
		$answer = '';
		
		$answer .= '<tr>';
			$answer .= '<th scope="row">'. @$count .'</th>';
			$answer .= '<td>'. $pupil['Pupil']['name_1'] .'</td>';
			$answer .= '<td>'. $pupil['Pupil']['name_2'] .'</td>';
			$answer .= '<td>'. $pupil['Pupil']['name_3'] .'</td>';
			
			if (!empty($pupil['Pupil']['birth'])) {
				$date = strftime("%d %B %Y", strtotime($pupil['Pupil']['birth']));
				if ($date[0] == '0')
					$date = substr($date, 1);
				$answer .= '<td>'. $date .'</td>';
			} else
				$answer .= '<td>Нет значения</td>';

			if (!empty($pupil['MyClass']['id']))
				$answer .= '<td>'. $pupil['MyClass']['number']. $pupil['MyClass']['letter'] .'</td>';
			else
				$answer .= '<td>Без класса</td>';

			$anket_count = count($pupil['Anket']);

			$answer .= '<td>'. $anket_count .'</td>';
			
			$name = $pupil['Pupil']['name_1'] .' '. $pupil['Pupil']['name_2'] .' '. $pupil['Pupil']['name_3'];
			
			$answer .= '<td class="button-area">';				

				if ($anket_count > 0) {

					$answer .= $this->Html->link(
						__(''),
						array('action' => 'showPupil/'.$pupil['Pupil']['id']),
						array('escape' => false, 'title' => "Просмотреть данные ученика", 'class' => 'button col-md-4 fa fa-search')
					);
					$answer .= $this->Html->link(
						__(''),
						array('action' => 'editPupil/'.$pupil['Pupil']['id']."/".$pupil['Pupil']['class_id']),
						array('escape' => false, 'title' => "Изменить данные ученика", 'class' => 'button col-md-4 fa fa-pencil')
					);
					$answer .= $this->Form->postLink(
						__(''),
						array('action' => 'deletePupil/'.$pupil['Pupil']['id']."/".$pupil['Pupil']['class_id']),
						array(
							'confirm' => 'Вы действительно хотите удалить ученика '.$name." из школы?",
							'escape' => false,
							'title' => "Удалить ученика из школы",
							'class' => 'button col-md-4 fa fa-times'
						)
					);
				} else {
					$answer .= $this->Html->link(
						__(''),
						'#',
						array('onclick' => "javascript:alert('Нет анкет. Просмотр данных невозможен.');", 'escape' => false, 'title' => "Просмотр данных невозможен", 'class' => 'button col-md-4 fa fa-exclamation-triangle')
					);
					$answer .= $this->Html->link(
						__(''),
						array('action' => 'editPupil/'.$pupil['Pupil']['id']."/".$pupil['Pupil']['class_id']),
						array('escape' => false, 'title' => "Изменить данные ученика", 'class' => 'button col-md-4 fa fa-pencil')
					);
					$answer .= $this->Form->postLink(
						__(''),
						array('action' => 'deletePupil/'.$pupil['Pupil']['id']."/".$pupil['Pupil']['class_id']),
						array(
							'confirm' => 'Вы действительно хотите удалить ученика '.$name." из школы?",
							'escape' => false,
							'title' => "Удалить ученика из школы",
							'class' => 'button col-md-4 fa fa-times'
						)
					);
				}

			$answer .= '</td>';
		$answer .= '</tr>';
		
		return $answer;
	}

	public function PupilsTableRowTask($pupil, $index, $tasks){	
		$answer = '';
			
		$name = $pupil['Pupil']['name_1'] .' '.mb_substr($pupil['Pupil']['name_2'],0,1) .'.'. mb_substr($pupil['Pupil']['name_3'],0,1) . '.';
		$name_full = $pupil['Pupil']['name_1'] .' '. $pupil['Pupil']['name_2'] .' '. $pupil['Pupil']['name_3'];
		
		$answer .= '<tr>';
			$answer .= '<th scope="row">'. $index .'</th>';
			$answer .= '<td title="' . $name_full . '">'. $name .'</td>';
			$count = 0;
			foreach ($tasks as $task) {
				$type =
					($task['level'] == 0) ? "success" :
					(
						($task['level'] == 1) ? "warning" :
							(
								($task['level'] == 2) ? "danger" : null
							)
					);

				$answer .= '<td class="'.$type.'" style="text-align: center; padding: 0;>';
					$answer .= $this->Form->input($pupil['Pupil']['id'].'.'.$task['level'].'.'.$task['id'], ['value'=>@$pupil['Pupil']['PupilTask'][$count]['mark'], 'label'=>false,
						'style'=>'width: 100%; text-align: center; padding: 7px 0; margin-bottom: 0px; background: none; border: none; border-radius: 0;',
						'class'=>'form-control', 'maxlength'=>'2', 'size'=>'2']);
				$answer .= '</td>';

				$count++;
			}

			if (isset($pupil['Pupil']['PupilResult']) && !empty($pupil['Pupil']['PupilResult'])) {
				$result = $pupil['Pupil']['PupilResult'][0]['count_e_m'];
				$answer .= '<td style="text-align: center;">' . $result . '</td>';

				$result = $pupil['Pupil']['PupilResult'][0]['success_percent_e_m'] . '%';
				$answer .= '<td style="text-align: center;">' . $result . '</td>';

				$result = $pupil['Pupil']['PupilResult'][0]['count_d'];
				$answer .= '<td style="text-align: center;">' . $result . '</td>';

				$result = $pupil['Pupil']['PupilResult'][0]['success_percent_d'] . '%';
				$answer .= '<td style="text-align: center;">' . $result . '</td>';

				$result = $pupil['Pupil']['PupilResult'][0]['score'];
				$answer .= '<td style="text-align: center;">' . $result . '</td>';

				$result = $pupil['Pupil']['PupilResult'][0]['success_percent'] . '%';
				$answer .= '<td style="text-align: center;">' . $result . '</td>';
			} else {
				$result = "-";
				$answer .= '<td style="text-align: center;">' . $result . '</td>';
				$answer .= '<td style="text-align: center;">' . $result . '</td>';
				$answer .= '<td style="text-align: center;">' . $result . '</td>';
				$answer .= '<td style="text-align: center;">' . $result . '</td>';
				$answer .= '<td style="text-align: center;">' . $result . '</td>';
				$answer .= '<td style="text-align: center;">' . $result . '</td>';
			}
		$answer .= '</tr>';
		
		return $answer;
	}

	public function PupilsTableRowSkill($pupil, $index, $questionsCount){	
		$answer = '';
			
		$name = $pupil['Pupil']['name_1'] .' '. mb_substr($pupil['Pupil']['name_2'],0,1) .'.'. mb_substr($pupil['Pupil']['name_3'],0,1) . '.';
		$name_full = $pupil['Pupil']['name_1'] .' '. $pupil['Pupil']['name_2'] .' '. $pupil['Pupil']['name_3'];
		
		$answer .= '<tr>';
			$answer .= '<th scope="row">'. $index .'</th>';
			$answer .= '<td title="' . $name_full . '">'. $name .'</td>';
			
			$count = 0;
			for($i=0; $i<$questionsCount; $i++) {
				$answer .= '<td style="text-align: center; padding: 0;>';
					$answer .= $this->Form->input($pupil['Pupil']['id'].'.'.$count, ['value'=>@$pupil['Pupil']['marks'][$count], 'label'=>false,
						'style'=>'width: 100%; text-align: center; padding: 7px 0; margin-bottom: 0px; background: none; border: none; border-radius: 0;',
						'class'=>'form-control', 'maxlength'=>'2', 'size'=>'2']);
				$answer .= '</td>';
				$count++;
			}
		$answer .= '</tr>';
		
		return $answer;
	}
	
	public function SemesterTableRow($semester, $count, $status = null){	
		$answer = '';
		
		if ($status == null) {
			$answer .= '<tr>';
		} else {
			$answer .= '<tr class="'.$status.'">';
		}	
				$answer .= '<th scope="row">'. @$count .'</th>';
				$answer .= '<td>'. $semester['Semester']['date_start'] .'</td>';
				$answer .= '<td>'. $semester['Semester']['date_end'] .'</td>';
				
				$answer .= '<td class="button-area">';				
					$answer .= $this->Html->link(
						__(''),
						array('action' => 'editSemester/'.$semester['Semester']['id']),
						array('escape' => false, 'title' => "Изменить период обучения", 'class' => 'button col-md-6 fa fa-pencil')
					);
					$answer .= $this->Form->postLink(
						__(''),
						array('action' => 'deleteSemester/'.$semester['Semester']['id']),
						array(
							'confirm' => 'Вы действительно хотите удалить период обучения №'.$count."?",
							'escape' => false,
							'title' => "Удалить период обучения",
							'class' => 'button col-md-6 fa fa-trash-o'
						)
					);
				$answer .= '</td>';
			$answer .= '</tr>';
		
		return $answer;
	}
	
	public function ClassTableRow($class, $count, $toggleName, $success = null){	
		$answer = '';

			if ($success == null) {
				$answer .= '<tr>';
			} else {
				$answer .= '<tr class="success">';
			}
				$answer .= '<th scope="row"  onclick="openTableRow('.$toggleName.')">'. @$count .'</th>';				
				$answer .= '<td onclick="openTableRow('.$toggleName.')">'. $class['MyClass']['number']. $class['MyClass']['letter'] .'</td>';
				$answer .= '<td onclick="openTableRow('.$toggleName.')">'. $class['Teacher']['name_1'] ." ". $class['Teacher']['name_2'] ." ". $class['Teacher']['name_3'] .'</td>';
				
				$answer .= '<td class="button-area">';
					$answer .= $this->Html->link(
						__(''),
						array('action' => 'addPupil/'.$class['MyClass']['id']."/list"),
						array('escape' => false, 'title' => "Добавить ученика в класс", 'class' => 'button col-md-3 fa fa-plus')
						);	
					$answer .= $this->Html->link(
						__(''),
						array('action' => 'showClass/'.$class['MyClass']['id']),
						array('escape' => false, 'title' => "Просмотр списка учеников", 'class' => 'button col-md-3 fa fa-search')
						);			
					$answer .= $this->Html->link(
						__(''),
						array('action' => 'editClass/'.$class['MyClass']['id']),
						array('escape' => false, 'title' => "Изменить данные класса", 'class' => 'button col-md-3 fa fa-pencil')
						);
					$answer .= $this->Form->postLink(__(''), array('action' => 'deleteClass/'.$class['MyClass']['id']), array(
						'confirm' => 'Вы действительно хотите распустить учеников класса '.$class['MyClass']['number'].$class['MyClass']['letter']."?",
						'escape' => false,
						'title' => "Распустить класс",
						'class' => 'button col-md-3 fa fa-trash-o'
						));
				$answer .= '</td>';
			$answer .= '</tr>';
		
		return $answer;
	}
	
	public function TeacherTableRow($teacher, $count){	
		$answer = '';

			$answer .= '<tr>';
				$answer .= '<th scope="row">'. @$count .'</th>';	
				$name = $teacher['Teacher']['name_1'] ." ". $teacher['Teacher']['name_2'] ." ". $teacher['Teacher']['name_3'];
				$answer .= '<td>'. $name .'</td>';
				if (!empty($teacher['Teacher']['class']))
					$answer .= '<td>'. $teacher['Teacher']['class']['MyClass']['number'] . $teacher['Teacher']['class']['MyClass']['letter'] .'</td>';
				else
					$answer .= '<td>Нет класса</td>';
				
				$answer .= '<td class="button-area">';	
					$answer .= $this->Html->link(
						__(''),
						array('action' => 'showTeacher/'.$teacher['Teacher']['id']),
						array('escape' => false, 'title' => "Просмотр данных учителя", 'class' => 'button col-md-4 fa fa-search')
						);			
					$answer .= $this->Html->link(
						__(''),
						array('action' => 'editTeacher/'.$teacher['Teacher']['id']),
						array('escape' => false, 'title' => "Изменить данные учителя", 'class' => 'button col-md-4 fa fa-pencil')
						);
					$answer .= $this->Form->postLink(__(''), array('action' => 'deleteTeacher/'.$teacher['Teacher']['id']), array(
						'confirm' => 'Вы действительно хотите удалить учителя '. $name ." из системы?",
						'escape' => false,
						'title' => "Удалить учителя из системы",
						'class' => 'button col-md-4 fa fa-trash-o'
						));
				$answer .= '</td>';
			$answer .= '</tr>';
		
		return $answer;
	}
	
	public function TypeTableRow($type, $count, $toggleName, $success = null){	
		$answer = '';
			
			if ($success == null) {
				$answer .= '<tr>';
			} else {
				$answer .= '<tr class="success">';
			}
				$answer .= '<th scope="row" onclick="openTableRow('.$toggleName.')">'. @$count .'</th>';
				$answer .= '<td onclick="openTableRow('.$toggleName.')">'. $type['Type']['name'] .'</td>';
				
				$answer .= '<td class="button-area">';	
					$answer .= $this->Html->link(
						__(''),
						array('action' => 'addSubject/'.$type['Type']['id']."/list"),
						array('escape' => false, 'title' => "Добавить новый предмет в данную категорию", 'class' => 'button col-md-3 fa fa-plus')
						);
					$answer .= $this->Html->link(
						__(''),
						array('action' => 'showType/'.$type['Type']['id']),
						array('escape' => false, 'title' => "Просмотр предметов данной категории", 'class' => 'button col-md-3 fa fa-search')
						);				
					$answer .= $this->Html->link(
						__(''),
						array('action' => 'editType/'.$type['Type']['id']),
						array('escape' => false, 'title' => "Изменить категорию", 'class' => 'button col-md-3 fa fa-pencil')
					);
					$answer .= $this->Form->postLink(
						__(''),
						array('action' => 'deleteType/'.$type['Type']['id']),
						array(
							'confirm' => 'Вы действительно хотите удалить категорию '.$type['Type']['name']." и все связанные с ней предметы?",
							'escape' => false,
							'title' => "Удалить категорию и все связанные с ней предметы",
							'class' => 'button col-md-3 fa fa-trash-o'
						)
					);
				$answer .= '</td>';
			$answer .= '</tr>';
		
		return $answer;
	}
	
	public function SubjectTableRow($subject, $count, $returnAction = null){	
		$answer = '';
		
			$answer .= '<tr>';
				$answer .= '<th scope="row">'. @$count .'</th>';
				$answer .= '<td>'. $subject['name'] .'</td>';
				$answer .= '<td>'. $subject['start_study_year'] .'</td>';
				$answer .= '<td>'. $subject['end_study_year'] .'</td>';
				
				$answer .= '<td class="button-area">';			
					$answer .= $this->Html->link(
						__(''),
						array('action' => 'editSubject/'.$subject['id']."/".$subject['type_id'].$returnAction),
						array('escape' => false, 'title' => "Изменить предмет", 'class' => 'button col-md-6 fa fa-pencil')
					);
					$answer .= $this->Form->postLink(
						__(''),
						array('action' => 'deleteSubject/'.$subject['id']."/".$subject['type_id'].$returnAction),
						array(
							'confirm' => 'Вы действительно хотите удалить предмет '.$subject['name']."?",
							'escape' => false,
							'title' => "Удалить предмет из данной категории",
							'class' => 'button col-md-6 fa fa-trash-o'
						)
					);
				$answer .= '</td>';
			$answer .= '</tr>';
		
		return $answer;
	}

	public function SubjectsTable($subjects, $returnType = null) {
		$answer = '';

		$answer .= '<table class="table table-striped table-hover pupil-list" style="margin-bottom: 0px;">';
			$answer .= '<thead>';
				$answer .= '<tr>';
					$answer .= '<th>#</th>';
					$answer .= '<th>Название</th>';
					$answer .= '<th title="Учебный год начала изучения предмета">Начало изучения</th>';
					$answer .= '<th title="Учебный год конца изучения предмета (включительно)">Конец изучения</th>';
					$answer .= '<th class="button-area">Действия</th>';
				$answer .= '</tr>';
				$answer .= '</thead>';
			$answer .= '<tbody>';
			
				$count = 0;
				foreach($subjects as $subject) {
					$count++;
					$answer .= $this->SubjectTableRow($subject, $count, $returnType);
				}
				
			$answer .= '</tbody>';
		$answer .= '</table>';

		return $answer;
	}

	public function PupilsTable($pupils, $returnType = null) {
		$answer = '';

		$answer .= '<table class="table table-striped table-hover pupil-list" style="margin-bottom: 0px;">';
			$answer .= '<thead>';
				$answer .= '<tr>';
					$answer .= '<th>#</th>';
					$answer .= '<th>Фамилия</th>';
					$answer .= '<th>Имя</th>';
					$answer .= '<th>Отчество</th>';
					$answer .= '<th>День рождения</th>';
					$answer .= '<th>Кол-во анкет</th>';
					$answer .= '<th class="button-area col-md-2">Действия</th>';
				$answer .= '</tr>';
				$answer .= '</thead>';
			$answer .= '<tbody>';

				$count = 0;
				foreach($pupils as $pupil) {
					$count++;
					$answer .= $this->PupilsTableRow($pupil, $count, $returnType);
				}
				
			$answer .= '</tbody>';
		$answer .= '</table>';

		return $answer;
	}
	
	public function TableFooterAdd($type = null){
		$answer = '';
		$answer .= '<div class="box-content tickets-new-details" style="padding: 0px;">';
			$answer .= '<div class="nav_table_specialities" style="background: rgba(26, 170, 217, 0.75)">';
				$answer .= '<ul class="nav_table_specialities_add_controls" tabindex="-1" data-reactid=".1.0.1.0">';
					$answer .= '<li>';
						$answer .= $this->Html->link(
													'<i class="icon-plus"></i> Добавить',													
													'javascript:speciality_add_show('.$type.')',
													array(
														'escape' => false,
														'class' => 'button_add'
													)
												);
					$answer .= '</li>';
		    	$answer .= '</ul>';
			$answer .= '</div>';
		$answer .= '</div>';
		
		return $answer;
	}

	public function rgb2hex($red, $green, $blue) {

		$red = dechex($red);
		if (strlen($red) < 2) {
			$red = "0".$red;
		}

		$green = dechex($green);
		if (strlen($green) < 2) {
			$green = "0".$green;
		}

		$blue = dechex($blue);
		if (strlen($blue) < 2) {
			$blue = "0".$blue;
		}

		$hex = "#".$red.$green.$blue;
		return $hex;
	}

	public function generateColors($count, $max_saturation) {

		$colors = array();

		$tmp_count = 0;
		$red = $max_saturation - 1;
		$green = 0;
		$blue = 0;

		$step = round($max_saturation * 6 / $count);

		while ($green < $max_saturation) {
			$colors[$tmp_count] = $this->rgb2hex($red, $green, $blue);
			$tmp_count++;
			$green += $step;
		}

		$red -= $green % $max_saturation;
		$green = $max_saturation - 1;

		while ($red > 0) {
			$colors[$tmp_count] = $this->rgb2hex($red, $green, $blue);
			$tmp_count++;
			$red -= $step;
		}

		$blue += 0 - $red;
		$red = 0;

		while ($blue < $max_saturation) {
			$colors[$tmp_count] = $this->rgb2hex($red, $green, $blue);
			$tmp_count++;
			$blue += $step;
		}

		$green -= $blue % $max_saturation;
		$blue = $max_saturation - 1;

		while ($green > 0) {
			$colors[$tmp_count] = $this->rgb2hex($red, $green, $blue);
			$tmp_count++;
			$green -= $step;
		}

		$red += 0 - $green;
		$green = 0;

		while ($red < $max_saturation) {
			$colors[$tmp_count] = $this->rgb2hex($red, $green, $blue);
			$tmp_count++;
			$red += $step;
		}

		$blue -= $red % $max_saturation;
		$red = $max_saturation - 1;

		while ($blue > 0) {
			$colors[$tmp_count] = $this->rgb2hex($red, $green, $blue);
			$tmp_count++;
			$blue -= $step;
		}

		$blue = 0;	

		return $colors;	
	}

	public function MakeSubjectChartNumberPHPArray($semesters_mod, $startNum) {
		$answer = array();
		$count = $startNum - 1;
		foreach ($semesters_mod as $semester) {
			foreach ($semester as $key_sem => $value_sem) {
				if ($key_sem != "anket_date") {

					if (!array_key_exists($key_sem, $answer)) {
						$count++;
						$answer[$key_sem] = array();
						$answer[$key_sem]['number'] = $count;
						$answer[$key_sem]['type'] = "type";	//идентификатор
					}

					foreach ($value_sem['Subject'] as $key_sub => $value_sub) {
						if (!array_key_exists($key_sub, $answer)) {
							$count++;
							$answer[$key_sub] = array();
							$answer[$key_sub]['number'] = $count;
							$answer[$key_sub]['type'] = "subject";	//идентификатор
						}
					}
				}
			}
		}

		$colors = $this->generateColors($count, 235);

		$tmp_count = 0;
		foreach ($answer as $key => $value) {
			$value['color'] = $colors[$tmp_count];
			$tmp_count++;
			$answer[$key] = $value;
		}

		return $answer;
	}

	public function MakeChartNumberPHPArray($semesters_mod, $startNum) {
		$answer = array();
		$count = $startNum - 1;
		foreach ($semesters_mod as $semester) {
			foreach ($semester as $key => $value) {
				if ($key != "anket_date") {
					if (!array_key_exists($key, $answer)) {
						$count++;
						$answer[$key] = array();
						$answer[$key]['number'] = $count;
					}
				}
			}
		}

		$colors = $this->generateColors($count, 235);

		$tmp_count = 0;
		foreach ($answer as $key => $value) {
			$value['color'] = $colors[$tmp_count];
			$tmp_count++;
			$answer[$key] = $value;
		}

		return $answer;
	}

	public function MakeChartPHPArray($semester_marks) {
		$answer = array();
		$answer['Subjects_data'] = array();
		$answer['Complex_data'] = array();
		$answer['Personal_data'] = array();

		foreach ($semester_marks as $semester) {

			$new_semester = array();
			//	по оси X - дата заполнения анкет
			//	"anket_date" : "2015-12-31",
			$new_semester['anket_date'] = $semester['Anket']['date'];

			$new_complex = array();
			$new_complex['anket_date'] = $semester['Anket']['date'];

			$new_personal = array();
			$new_personal['anket_date'] = $semester['Anket']['date'];

			foreach ($semester['Type'] as $type) {

				//	средний балл категории за этот семестр
				//	"Русский язык" : 3.5,
				$new_semester[$type['name']] = array();
				$new_semester[$type['name']]['mark'] = $type['type_mark'];
				$new_semester[$type['name']]['Subject'] = array();

				foreach ($type['Subject'] as $subject) {
					if ($subject['Mark'] != null) {
						//	"Физика" : 5,
						$new_semester[$type['name']]['Subject'][$subject['name']] = $subject['Mark']['mark'];
					}
				}

			};

			//	данные для комплексного тестирования
			foreach ($semester['Anket'] as $key => $value) {

				if (($key != "id") && ($key != "pupil_id") && ($key != "teacher_id") && ($key != "semester_id") && ($key != "date")) {

					if ($key == "mark_complex") {						
						$new_complex[$key] = $value;
					} else {
						$new_personal[$key] = $value;
					}

				}
			}

			//	данные для личностных достижений
			foreach ($semester['Personal_data_anket'] as $personal_data_field) {

				$new_personal[$personal_data_field['Personal_data_field']['name']] = $personal_data_field['Personal_data_answer']['weight'];

			};

			array_push($answer['Subjects_data'], $new_semester);
			array_push($answer['Complex_data'], $new_complex);
			array_push($answer['Personal_data'], $new_personal);
		}

		return $answer;
	}

	public function MakeChartDataFromSubject($semesters_mod, $number) {
		$answer = '';

		$answer .= 'var chartData'.$number.' = [';

		$massive = '';

		foreach ($semesters_mod as $semester) {

			$massive .= '{';

			//	переводим каждый из пунктов
			foreach ($semester as $key => $value) {

				if ($key == "anket_date") {				
					$massive .= '"'.$key.'"'." : " . '"'.$value.'"'.",";
				} else {
					$massive .= '"'.$key.'"'." : " . '"'.$value['mark'].'"'.",";
					foreach ($value['Subject'] as $key_sub => $value_sub) {
						$massive .= '"'.$key_sub.'"'." : " . '"'.$value_sub.'"'.",";
					}
				}
			}

			$massive = substr($massive, 0, -1);	//удаляем последнюю запятую из массива текущего семестра
			$massive .= '},';

		}

		$answer .= substr($massive, 0, -1);	//удаляем последнюю запятую из всего массива
		$answer .= '];';

		return $answer;
	}

	public function MakeChartDataFrom($semesters_mod, $number) {
		$answer = '';

		$answer .= 'var chartData'.$number.' = [';

		$massive = '';

		foreach ($semesters_mod as $semester) {

			$massive .= '{';

			//	переводим каждый из пунктов
			foreach ($semester as $key => $value) {
				
				$massive .= '"'.$key.'"'." : " . '"'.$value.'"'.",";
			}

			$massive = substr($massive, 0, -1);	//удаляем последнюю запятую из массива текущего семестра
			$massive .= '},';

		}

		$answer .= substr($massive, 0, -1);	//удаляем последнюю запятую из всего массива
		$answer .= '];';

		return $answer;
	}

	// public function NewChartDataMark($array_marks, $semesters, $number) {
	// 	$answer = '';

	// 	$answer .= 'var chartData'.$number.' = [';

	// 	$massive = '';

	// 	foreach ($semesters as $semester) {
	// 		$count_marks = 0;
	// 		if (!empty($array_marks)) {
	// 		 	foreach ($array_marks as $mark) {
	// 		 		if ($semester['anket_id'] == $mark['anket_id']) {
	// 					$massive .= '{';
	// 					$massive .= '"name"'." : " . '"' . $semester['anket_date'].'"'.",";
	// 					$massive .= '"mark"'." : " . $mark['mark'];
	// 					$massive .= '},';
	// 					$count_marks++;
	// 		 		};
	// 		 	}
	// 	 	}
	// 	 	if ($count_marks == 0) {
	// 	 		$massive .= '{';
	// 			$massive .= '"name"'.' : ' . '"' . $semester['anket_date'].'"';
	// 			$massive .= '},';
	// 	 	}
	// 	}
	// 	$answer .= substr($massive, 0, -1);	//удаляем последнюю запятую

	// 	$answer .= '];';

	// 	return $answer;
	// }

	public function GetChartSettings($graphsNumberAndColors, $number) {
		$answer = '';

		//-----------------
		//НАСТРОЙКИ ГРАФИКА
		$chartName = "chart".$number;	//имя
		$chartDataName = "chartData".$number;	//имя источника данных

		$answer .= $chartName.' = new AmCharts.AmSerialChart();';	//создание
		$answer .= $chartName.'.dataProvider = '.$chartDataName.";";	//назначение источника данных
		$answer .= $chartName.'.categoryField = "anket_date";
		';	//назначение имен горизонтальной оси определенному полю
		//$answer .= $chartName.'.startDuration = 0.35;';	//скорость анимации появления
		//$answer .= $chartName.'.startEffect = ">";';	//анимация появления

		//---------------------------------
		//НАСТРОЙКИ ВЕРТИКАЛЬНОЙ ОСИ ОЦЕНОК
		$valAxisName = "yAxis".$number;	//имя

		$answer .= 'var '.$valAxisName.' = new AmCharts.ValueAxis();';	//создание
		$answer .= $valAxisName.'.position = "left";';	//позиция оси слева
		$answer .= $valAxisName.'.autoGridCount = false;';	//назначаем фиксированные значения
		$answer .= $valAxisName.'.gridCount = 6;';	//назначаем кол-во горизонтальных линий
		$answer .= $valAxisName.'.minimum = 0;';	//минимальное значение
		$answer .= $valAxisName.'.maximum = 6;';	//максимальное значение
		$answer .= $valAxisName.'.title = "Оценка";';	//название оси
		$answer .= $valAxisName.'.showFirstLabel = false;';	//убираем первое значение
		$answer .= $valAxisName.'.showLastLabel = false;';	//убираем последнее значение

		$answer .= $chartName.'.addValueAxis('.$valAxisName.');
		';	//назначение оси графику

		//----------------------------------------------
		//НАСТРОЙКИ ГОРИЗОНТАЛЬНОЙ ОСИ УЧЕБНЫХ ЧЕТВЕРТЕЙ
		$categoryAxisName = 'categoryAxis'.$number;	//имя

		$answer .= 'var '.$categoryAxisName.' = '.$chartName.'.categoryAxis;';	//создание
		// $answer .= '	'.$categoryAxisName.'.labelRotation = 45;';	//поворот меток
		$answer .= $categoryAxisName.'.autoGridCount  = false;';	//назначаем фиксированные значения
		$answer .= $categoryAxisName.'.gridCount = '.$chartDataName.'.length;';  	//назначаем кол-во вертикальных линий               
		$answer .= $categoryAxisName.'.gridPosition = "middle";';	//положение горизонтальных линий относительно меток
		//$answer .= $categoryAxisName.'.title = chartDataName;';	//положение горизонтальных линий относительно меток

		//---------------------
		//НАСТРОЙКИ SCROLLBAR'а
		// $scrollName = 'scroll'.$number;	//имя

		// $answer .= $scrollName.' = new AmCharts.ChartScrollbar();';	//создание
		// $answer .= $scrollName.'.dragIcon = "dragIcon";';	//ползунок

		// $answer .= $chartName.'.addChartScrollbar('.$scrollName.');';	//назначение scrollbar'а графику

		$tmp_count = 1;
		foreach ($graphsNumberAndColors as $key => $value) {
			//	$value => {["number"], ["color"]}

			//---------------
			//НАСТРОЙКИ ГРАФА
			$graphName = 'graph'.$value["number"];	//имя

			$answer .= $graphName.' = new AmCharts.AmGraph();';	//создание
			$answer .= $graphName.'.valueField = "'.$key.'";';	//назначение поля источника данных
			$answer .= $graphName.'.balloonText = "[[category]]: оценка=<b>[[value]]</b>";';	//назачение формата текста при наведении
			$answer .= $graphName.'.type = "line";';	//тип графа
			$answer .= $graphName.'.bullet = "round";';	//тип точек
			$answer .= $graphName.'.lineColor = "'.$value["color"].'";';	//цвет графа

			if ($tmp_count > 1) {
				$answer .= $graphName.'.hidden = true;';	//скрываем все графы, кроме первого
			};
			$tmp_count++;

			$answer .= $chartName.'.addGraph('.$graphName.');
			';	//назначение графа графику

		}

		//-------------------
		//НАСТРОЙКИ ОТРИСОВКИ
		$chartDivID = $chartName.'div';	//имя блока отрисовки

		$answer .= $chartName.'.write("'.$chartDivID.'");
		';	//назначение блока отрисовки

		return $answer;
	}

	// public function NewChartSettings($number, $color) {
	// 	$answer = '';

	// 	//-----------------
	// 	//НАСТРОЙКИ ГРАФИКА
	// 	$chartName = "chart".$number;	//имя
	// 	$chartDataName = "chartData".$number;	//имя источника данных

	// 	$answer .= $chartName.' = new AmCharts.AmSerialChart();';	//создание
	// 	$answer .= $chartName.'.dataProvider = '.$chartDataName.";";	//назначение источника данных
	// 	$answer .= $chartName.'.categoryField = "name";';	//назначение имен горизонтальной оси определенному полю
	// 	//$answer .= $chartName.'.startDuration = 0.35;';	//скорость анимации появления
	// 	//$answer .= $chartName.'.startEffect = ">";';	//анимация появления

	// 	//---------------------------------
	// 	//НАСТРОЙКИ ВЕРТИКАЛЬНОЙ ОСИ ОЦЕНОК
	// 	$valAxisName = "yAxis".$number;	//имя

	// 	$answer .= 'var '.$valAxisName.' = new AmCharts.ValueAxis();';	//создание
	// 	$answer .= $valAxisName.'.position = "left";';	//позиция оси слева
	// 	$answer .= $valAxisName.'.autoGridCount = false;';	//назначаем фиксированные значения
	// 	$answer .= $valAxisName.'.gridCount = 6;';	//назначаем кол-во горизонтальных линий
	// 	$answer .= $valAxisName.'.minimum = 0;';	//минимальное значение
	// 	$answer .= $valAxisName.'.maximum = 6;';	//максимальное значение
	// 	$answer .= $valAxisName.'.title = "Оценка";';	//название оси
	// 	$answer .= $valAxisName.'.showFirstLabel = false;';	//убираем первое значение
	// 	$answer .= $valAxisName.'.showLastLabel = false;';	//убираем последнее значение

	// 	$answer .= 'chart'.$number.'.addValueAxis('.$valAxisName.');';	//назначение оси графику

	// 	//----------------------------------------------
	// 	//НАСТРОЙКИ ГОРИЗОНТАЛЬНОЙ ОСИ УЧЕБНЫХ ЧЕТВЕРТЕЙ
	// 	$categoryAxisName = 'categoryAxis'.$number;	//имя

	// 	$answer .= 'var '.$categoryAxisName.' = '.$chartName.'.categoryAxis;';	//создание
	// 	// $answer .= '	'.$categoryAxisName.'.labelRotation = 45;';	//поворот меток
	// 	$answer .= $categoryAxisName.'.autoGridCount  = false;';	//назначаем фиксированные значения
	// 	$answer .= $categoryAxisName.'.gridCount = '.$chartDataName.'.length;';  	//назначаем кол-во вертикальных линий               
	// 	$answer .= $categoryAxisName.'.gridPosition = "middle";';	//положение горизонтальных линий относительно меток
	// 	$answer .= $categoryAxisName.'.title = chartDataName'.$number.';';	//положение горизонтальных линий относительно меток

	// 	//---------------------
	// 	//НАСТРОЙКИ SCROLLBAR'а
	// 	// $scrollName = 'scroll'.$number;	//имя

	// 	// $answer .= $scrollName.' = new AmCharts.ChartScrollbar();';	//создание
	// 	// $answer .= $scrollName.'.dragIcon = "dragIcon";';	//ползунок

	// 	// $answer .= $chartName.'.addChartScrollbar('.$scrollName.');';	//назначение scrollbar'а графику

	// 	//---------------
	// 	//НАСТРОЙКИ ГРАФА
	// 	$graphName = 'graph'.$number;	//имя

	// 	$answer .= $graphName.' = new AmCharts.AmGraph();';	//создание
	// 	$answer .= $graphName.'.valueField = "mark";';	//назначение поля источника данных
	// 	$answer .= $graphName.'.balloonText = "[[category]]: оценка=<b>[[value]]</b>";';	//назачение формата текста при наведении
	// 	$answer .= $graphName.'.type = "line";';	//тип графа
	// 	$answer .= $graphName.'.bullet = "round";';	//тип точек
	// 	$answer .= $graphName.'.lineColor = "'.$color.'";';	//цвет графа
	// 	$answer .= $chartName.'.addGraph('.$graphName.');';	//назначение графа графику

	// 	//-------------------
	// 	//НАСТРОЙКИ ОТРИСОВКИ
	// 	$chartDivID = $chartName.'div';	//имя блока отрисовки

	// 	$answer .= $chartName.'.write("'.$chartDivID.'");';	//назначение блока отрисовки

	// 	return $answer;
	// }
	
}
?>