<h2><?php echo $title_for_layout; ?></h2>

<div class="contact-form">
<?php
	echo $this->Form->create('Teacher', array('action' => $formAction, 'default' => true, 'id' => 'ClassChooseForm'));
	
	echo '<div class="inputs-left">';
	$options = array(
		array(
            'name' => 'Нет классов',
            'value' => 'null',
            'disabled' => false,
            'selected' => false
        )
    );

    if ($classes_data != null)
        	$options = $classes_data;

	echo $this->Form->input('class_id', array(
		'type' => 'select',
		'class' => 'form-control',
		'label' => 'Класс',
		'options' => @$options,
		'selected' => @$pupil_data['MyClass']['id']
	));
		
	echo '</div>';
	
	echo $this->Form->submit('Выбрать класс', array('class' => 'btn btn-primary button', 'div' => false, 'style' => 'margin-right: 25px;'));
	echo $this->Form->end();
?>
</div>