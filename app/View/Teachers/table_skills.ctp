﻿<?php if (!empty($classes) && $classes[0]['Pupil'] && !empty($classes[0]['Pupil'])) : ?>
	<div class="contact-form">
		<?php echo $this->Form->create('Teacher', array('action' => $formAction, 'default' => true, 'id' => 'ClassChooseForm')); ?>

			<table class="table table-hover pupil-list" style="margin-bottom: 0px;">
				<thead>
					<tr>
						<th rowspan="2">#</th>
						<th rowspan="2">ФИО</th>
						<th colspan="5" style="text-align: center">Навыки и умения ученика</th>
					</tr>
					<tr>
						<th>Умение находить верные решения при помощи единичной числовой окружности</th>
						<th>Умение пользоваться тригонометрическими формулами</th>
						<th>Знание свойств тригонометрических функций</th>
						<th>Умение использовать различные методы при решении уравнений</th>
						<th>Умение работать со степенями числа</th>
					</tr>
				</thead>
				<tbody>
				
					<?php
						$index = 0;
						foreach($classes[0]['Pupil'] as $pupil) {
							$index++;
							$pupil = array('Pupil' => $pupil);
							echo $this->Widgets->PupilsTableRowSkill($pupil, $index, 5);
						}
					?>
					
				</tbody>
			</table>
		<?php
			echo $this->Form->submit('Готово', array('class' => 'btn btn-primary button', 'div' => false, 'style' => 'margin-right: 25px;'));
			echo $this->Html->link('Таблица результатов', array('action' => $anotherAction), array('class' => 'btn btn-primary button', 'div' => false));
			echo $this->Form->end();
		?>
	</div>
<?php else: ?>
	<?php echo $this->element('no-pupils-found'); ?>
<?php endif; ?>
<?php //debug(@$result); ?>
<?php //debug(@$classes); ?>