﻿<table class="table table-hover pupil-list" style="margin-bottom: 0px;">
	<thead>
		<tr>
			<th>Название</th>
		</tr>
	</thead>
	<tbody>
	
		<?php
			foreach($classes['WorkTable'] as $workTable) {
				echo $this->Widgets->TableRowWorkTable($workTable);
			}
		?>
		
	</tbody>
</table>
<div class="contact-form">
<?php echo $this->Html->link('Новая работа', array('action' => 'addWorkTable/'.$class_id), array('class' => 'btn btn-primary button', 'div' => false)); ?>
</div>