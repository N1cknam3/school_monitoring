﻿<?php
	/*
		ВООБЩЕ ТУТ ДОЛЖЕН БЫТЬ СПИСОК РАБОТ КЛАССА С ВОЗМОЖНОСТЬЮ ДОБАВЛЕНИЯ
		НО ПОКА ТУТ БУДЕТ ОДНА ТАБЛИЦА
	*/
	
	/*
		Вывод списка работ этого класса с возможностью добавления новой работы.
		
		Новая работа - это ввод кол-ва простых, средних и сложных заданий (по цвету их визуально разделить) и все.

		В самой таблице предусмотреть ввод баллов за задание.
		Номер задания - по порядку.
		Ученики выводятся по списку в классе (по алфавиту).
		Заголовки вертикальные сделать горизонтальными с подсказкой при наведении.
		Содержимое итогов - пустое и не подлежит изменению.
		По нажатию "ГОТОВО" возврат в эту же ф-ию и обработка результатов, а потом вывод в эти ячейки только для чтения.
		То же и с ячейками внизу таблицы.

		Внизу еще есть описание заданий и описание умений к этим заданиям - сделать простыми формами внизу.

		Справа ставится еще итоговая оценка за работу учителем.

		ИТОГО:
		сама работа
			work_tables:
				id
				class_id
				name - название

		список заданий работы:
			tasks:
				id
				work_table_id - id работы
				number - номер задания (выводиться будут по нему)
				level - уровень 0, 1, 2
				mark - кол-во баллов за задание
				description - элементы содержания
				skill - объект контроля (вид знаний и умений)

		проставленные баллы в таблице - это просто связь ученика и задания:
			pupil_tasks:
				id
				pupil_id
				task_id
				mark - кол-во полученных баллов за задание

		итоги сохраняются просто в отдельную таблицу для удобства дальнейшего отображения:
			pupil_results:
				id
				pupil_id
				success_percent - % успешности выполнения работы (округление до целого)
				mark - итоговая оценка за работу
	*/

	
?>

<?php if (!empty($work_table)): ?>

	<?php

		$questions_count_easy = 0;
		$questions_count_middle = 0;
		$questions_count_difficult = 0;

		foreach ($work_table['Task'] as $task) {
			if ($task['level'] == 0)
				$questions_count_easy++;
			if ($task['level'] == 1)
				$questions_count_middle++;
			if ($task['level'] == 2)
				$questions_count_difficult++;
		}

		$questions_count = $questions_count_easy+$questions_count_middle+$questions_count_difficult;

		$classes = $work_table['MyClass'];

	?>

	<div class="contact-form">
		<?php echo $this->Form->create('Teacher', array('action' => $formAction, 'default' => true, 'id' => 'ClassChooseForm')); ?>

			<table class="table table-hover pupil-list" style="margin-bottom: 0px;">
				<thead>
					<?php if (false) : ?>
						<tr>
							<th></th>
							<th></th>
							<?php
								echo '<th style="text-align: center; padding: 0;" colspan="'.$questions_count_easy.'">';
								echo "1 уровень (репродуктивн.)";
								echo '</th>';
							?>
							<?php
								echo '<th style="text-align: center; padding: 0;" colspan="'.$questions_count_middle.'">';
								echo "2 уровень (выполнение действий в знакомой ситуации, по стандартному алгоритму)";
								echo '</th>';
							?>
							<?php
								echo '<th style="text-align: center; padding: 0;" colspan="'.$questions_count_difficult.'">';
								echo "3 уровень (применение знаний в нестандартной ситуации)";
								echo '</th>';
							?>
							<th></th>
						</tr>
					<?php endif; ?>
					<tr>
						<th></th>
						<th>Макс. балл</th>
						<?php
							foreach($work_table['Task'] as $task) {
								echo '<th style="text-align: center; padding: 0;">';
								echo $this->Form->input('mark.'.$task['level'].'.'.$task['id'], ['value'=>$task['mark'], 'label'=>false,
										'style'=>'width: 100%; text-align: center; padding: 7px 0; margin-bottom: 0px; background: none; border: none; border-radius: 0;',
										'class'=>'form-control', 'maxlength'=>'2', 'size'=>'2']);
								echo '</th>';
							}
						?>
						<th></th>
					</tr>
					<tr>
						<th>#</th>
						<th>№ задания</th>
						<?php
							for($i=1; $i<=$questions_count; $i++) {
								echo '<th style="text-align: center;">' . $i . '</th>';
							}
						?>
						<th style="text-align: center" title="Кол-во полностью решенных уравнений 1 и 2 уровней">A</th>
						<th style="text-align: center" title="% полностью решенных заданий 1 и 2 уровней">B</th>
						<th style="text-align: center" title="Кол-во полностью решенных уравнений 3 уровня">C</th>
						<th style="text-align: center" title="% полностью решенных заданий 3 уровня">D</th>
						<th style="text-align: center" title="Всего баллов за работу">E</th>
						<th style="text-align: center" title="% успешности">F</th>
					</tr>
				</thead>
				<tbody>
				
					<?php
						$index = 0;
						foreach($classes['Pupil'] as $pupil) {
							$index++;
							$pupil = array('Pupil' => $pupil);
							echo $this->Widgets->PupilsTableRowTask($pupil, $index, $work_table['Task']);
						}
					?>
					
				</tbody>
			</table>
		<?php
			echo $this->Form->submit('Готово', array('class' => 'btn btn-primary button', 'div' => false, 'style' => 'margin-right: 25px;'));
			echo $this->Html->link('Таблица навыков', array('action' => $anotherAction), array('class' => 'btn btn-primary button', 'div' => false));
			echo $this->Form->end();
		?>
	</div>
<?php else: ?>
	<?php echo $this->element('no-pupils-found'); ?>
<?php endif; ?>


<div style="text-align: left;">
	<?php //debug(@$work_table); ?>
</div>


<style>
#chartdiv {
	width		: 100%;
	height		: 500px;
	font-size	: 11px;
}					
</style>

<!-- Resources -->


<!-- Chart code -->
<script>
var chart = AmCharts.makeChart( "chartdiv", {
  "type": "serial",
  "theme": "light",
  "dataProvider": 

  [
  	<?php
  		$data = "";
  		foreach ($classes['Pupil'] as $pupil) {
  			$name = $pupil['name_1'] . ' ' . mb_substr($pupil['name_2'], 0, 1) . '. ' . mb_substr($pupil['name_3'], 0, 1);
  			$data .= '{ "name" : "' . $name . '",';
  			$data .= '"result" : "' . @$pupil['PupilResult'][0]['success_percent'] . '" },';
  		}
  		echo $data;
  	?>
  ]

  ,
  "valueAxes": [ {
    "gridColor": "#FFFFFF",
    "gridAlpha": 0.2,
    "dashLength": 0
  } ],
  "gridAboveGraphs": true,
  "startDuration": 1,
  "graphs": [ {
    "balloonText": "[[name]]: <b>[[result]]</b>",
    "fillAlphas": 0.8,
    "lineAlpha": 0.2,
    "type": "column",
    "valueField": "result"
  } ],
  "chartCursor": {
    "categoryBalloonEnabled": false,
    "cursorAlpha": 0,
    "zoomable": false
  },
  "categoryField": "name",
  "categoryAxis": {
    "gridPosition": "start",
    "gridAlpha": 0,
    "tickPosition": "start",
    "tickLength": 20,
    "labelRotation": 45,
    "autoGridCount": false,
    "gridCount": 25
  },
  "export": {
    "enabled": true
  },
  "valueAxes": [{
    "autoGridCount": false,
    "gridCount": 10,
    "minimum": 0,
    "maximum": 100
  }]

} );
</script>

<!-- HTML -->
<div id="chartdiv"></div>