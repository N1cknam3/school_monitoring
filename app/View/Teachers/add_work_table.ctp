<h2><?php echo $title_for_layout; ?></h2>

<div class="contact-form">
<?php
	echo $this->Form->create('Teacher', array('action' => $formAction, 'default' => true, 'id' => 'TeacherEditForm'));
	
	echo '<div class="inputs-left">';
	echo $this->Form->input('name', array(
		'type' => 'text',
		'class' => 'form-control',
		'label' => 'Название',
		'value' => "Новая работа",
		'placeholder' => 'Введите название работы'
	));
	echo $this->Form->input('count_easy', array(
		'type' => 'text',
		'class' => 'form-control',
		'label' => '1й раздел',
		'value' => 2,
		'placeholder' => 'Введите кол-во вопросов 1го раздела'
	));
	echo $this->Form->input('count_middle', array(
		'type' => 'text',
		'class' => 'form-control',
		'label' => '2й раздел',
		'value' => 5,
		'placeholder' => 'Введите кол-во вопросов 2го раздела'
	));
	echo $this->Form->input('count_difficult', array(
		'type' => 'text',
		'class' => 'form-control',
		'label' => '3й раздел',
		'value' => 3,
		'placeholder' => 'Введите кол-во вопросов 3го раздела'
	));
	
	echo '</div>';
	
	echo $this->Form->submit('Сохранить', array('class' => 'btn btn-primary button', 'div' => false, 'style' => 'margin-right: 25px;'));
	echo $this->Html->link('Вернуться', array('action' => $returnAction), array('class' => 'btn btn-primary button', 'div' => false));
	echo $this->Form->end();
?>
</div>