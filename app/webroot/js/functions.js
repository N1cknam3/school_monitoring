﻿function ajax_login() {
	if ($('.message').text() != 'ok') {
		$('#status').fadeIn();
	} else {
		$('#status').fadeOut();
		window.location.href = $("#login_route").text();
	}
}

$(document).ready(function(){
	
	setTimeout(function() { $(".menu").slideToggle(800); }, 1000);
	
});

function openTableRow(element) {
	if (!$(element).is(':visible')) {
		$(element).css('display', 'table-row');
		$(element+'-div').slideToggle(500);
		//$(element+'-div').css('display', 'block');
		//animateElement(element+'-div', 'pulse');
	} else {
		$(element+'-div').slideToggle(700);
		//animateElement(element+'-div', 'fadeOut');
		$(function() {
		 	setTimeout(function() {
		 		//$(element+'-div').css('display', 'block');
		  		$(element).css('display', 'none');
			}, 700);
		});		
	}
}

//управление графами с помощью checkbox'ов
$(document).on("click", "input[type='checkbox']", function(){

   	chartName = $(this).attr('chart');
    graphName = $(this).attr('graph');

    if (!window[graphName].hidden)
    	window[chartName].hideGraph(window[graphName]);
    else
    	window[chartName].showGraph(window[graphName]);


    if ($(this).parent().hasClass("active"))
    	$(this).parent().removeClass("active");
    else
    	$(this).parent().addClass("active");

});

// //управление графами с помощью radioButton'ов
// $(document).on("change", "input[type='radio']", function(){

// 	//получаем номер этого предмета
// 	name = $(this).attr('id');
//     number = name.replace("chartRadio", "");

// 	chartDivID = "#chart"+number+"div";

//     if (!$(chartDivID).is(':visible')) {
// 		closeAllCharts();
// 		$(chartDivID).fadeIn();
// 	}

// });

// //управление графами с помощью radioButton'ов
// $(document).on("click", "div.radioDiv", function(){

// 	//получаем номер этого предмета
// 	name = $(this).attr('id');
//     number = name.replace("radioDiv", "");

//     $("#chartRadio"+number).click();

// });

//управление графами с помощью radioButton'ов
$(document).on("click", "div.checkDiv", function(){

	//получаем номер этого предмета
	name = $(this).attr('id');
    number = name.replace("checkDiv", "");

    $("#chartCheck"+number).click();

});

function closeAllCharts() {

	$(document).find("div.chartDiv").css('display', 'none');

}

function openGraph(element) {

	if (!$(element).is(':visible')) {
		closeAllCharts();
		$(element).fadeIn();
	}
}

function animateElement(elementName, animationName) {

	$(function() {
		setTimeout(function() {
			$(elementName).addClass(animationName + " animated");
		}, 0);
	});
		
	$(function() {
		setTimeout(function() {
			$(elementName).removeClass(animationName + " animated")
		}, 1000);
	});

}