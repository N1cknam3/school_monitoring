<?php
App::uses('AppModel', 'Model');
/**
 * Pupil Model
 *
 *
 *
 */
class Pupil extends AppModel {

	public $actsAs = array('Containable');

	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'MyClass' => array(
			'className' => 'MyClass',
			'foreignKey' => 'class_id',
			'conditions' => '',
			'fields' => '',
			'order' => array(
				'number' => 'ASC',
				'letter' => 'ASC'
			)
		)
	);

/**
 * hasMany associations
 *
 * @var array
 */

	public $hasMany = array(
		'Anket' => array(
			'className' => 'Anket',
			'foreignKey' => 'pupil_id'
			// ,'dependent' => false,
			// 'conditions' => '',
			// 'fields' => '',
			// 'order' => '',
			// 'limit' => '',
			// 'offset' => '',
			// 'exclusive' => '',
			// 'finderQuery' => '',
			// 'counterQuery' => ''
		),
		'PupilTask' => array(
			'className' => 'PupilTask',
			'foreignKey' => 'pupil_id'
		),
		'PupilResult' => array(
			'className' => 'PupilResult',
			'foreignKey' => 'pupil_id'
		)

	);

/**
 * hasAndBelongsToMany associations
 *
 * @var array
 */
	public $hasAndBelongsToMany = array(
        'Task' =>
            array(
                'className' => 'Task',
                'joinTable' => 'pupil_tasks',
                'foreignKey' => 'pupil_id',
                'associationForeignKey' => 'task_id'
                // ,'unique' => true,
                // 'conditions' => '',
                // 'fields' => '',
                // ,'order' => ''
                // 'limit' => '',
                // 'offset' => '',
                // 'finderQuery' => '',
                // 'with' => ''
            )
    );


}
