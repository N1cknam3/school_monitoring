<?php
App::uses('AppModel', 'Model');
/**
 * Task Model
 *
 *
 *
 */
class WorkTable extends AppModel {

	public $actsAs = array('Containable');

	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
    public $belongsTo = array(
        'MyClass' => array(
            'className' => 'MyClass',
            'foreignKey' => 'class_id',
            // 'conditions' => '',
            // 'fields' => ''
            // 'order' => ''
        )
    );

/**
 * hasMany associations
 *
 * @var array
 */

    public $hasMany = array(
        'Task' => array(
            'className' => 'Task',
            'foreignKey' => 'work_table_id'
            // 'dependent' => false,
            // 'conditions' => '',
            // 'fields' => '',
            // ,'order' => ['name_1', 'name_2', 'name_3'],
            // 'limit' => '',
            // 'offset' => '',
            // 'exclusive' => '',
            // 'finderQuery' => '',
            // 'counterQuery' => ''
        )
    );


}
