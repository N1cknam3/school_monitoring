<?php
App::uses('AppModel', 'Model');
/**
 * Personal_data_anket Model
 *
 *
 *
 */
class Personal_data_anket extends AppModel {

	public $actsAs = array('Containable');
	
    //public $useTable = 'classes';

	//public $order = array("MyClass.number" => "ASC", "MyClass.letter" => "ASC");

	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'Personal_data_field' => array(
			'className' => 'Personal_data_field',
			'foreignKey' => 'personal_data_field_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'Personal_data_answer' => array(
			'className' => 'Personal_data_answer',
			'foreignKey' => 'personal_data_answer_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);
	
/*
	public function beforeFind($queryData) {
	    $sticky = array('letter' => 'ASC');
	
	    if (is_array($queryData['order'][0])) {
	        $queryData['order'][0] = $sticky + $queryData['order'][0];
	    }
	    else {
	        $queryData['order'][0] = $sticky;
	    }
	
	    return $queryData;
	}
*/

/**
 * hasMany associations
 *
 * @var array
 */

	// public $hasMany = array(
	// 	'Mark' => array(
	// 		'className' => 'Mark',
	// 		'foreignKey' => 'anket_id',
	// 		'dependent' => false,
	// 		'conditions' => '',
	// 		'fields' => '',
	// 		'order' => '',
	// 		'limit' => '',
	// 		'offset' => '',
	// 		'exclusive' => '',
	// 		'finderQuery' => '',
	// 		'counterQuery' => ''
	// 	),
	// 	'Personal_data_anket' => array(
	// 		'className' => 'Personal_data_anket',
	// 		'foreignKey' => 'anket_id',
	// 		'dependent' => false,
	// 		'conditions' => '',
	// 		'fields' => '',
	// 		'order' => '',
	// 		'limit' => '',
	// 		'offset' => '',
	// 		'exclusive' => '',
	// 		'finderQuery' => '',
	// 		'counterQuery' => ''
	// 	),
	// );

}
