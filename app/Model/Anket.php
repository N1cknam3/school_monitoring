<?php
App::uses('AppModel', 'Model');
/**
 * Anket Model
 *
 *
 *
 */
class Anket extends AppModel {
	
	public $actsAs = array('Containable');
	
    //public $useTable = 'classes';

	//public $order = array("MyClass.number" => "ASC", "MyClass.letter" => "ASC");

	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'Pupil' => array(
			'className' => 'Pupil',
			'foreignKey' => 'pupil_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'Semester' => array(
			'className' => 'Semester',
			'foreignKey' => 'semester_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);
	
/*
	public function beforeFind($queryData) {
	    $sticky = array('letter' => 'ASC');
	
	    if (is_array($queryData['order'][0])) {
	        $queryData['order'][0] = $sticky + $queryData['order'][0];
	    }
	    else {
	        $queryData['order'][0] = $sticky;
	    }
	
	    return $queryData;
	}
*/

/**
 * hasMany associations
 *
 * @var array
 */

	public $hasMany = array(
		'Mark' => array(
			'className' => 'Mark',
			'foreignKey' => 'anket_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		),
		'Personal_data_anket' => array(
			'className' => 'Personal_data_anket',
			'foreignKey' => 'anket_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		),
	);

}
