<?php
App::uses('AppModel', 'Model');
/**
 * Class Model
 *
 *
 *
 */
class MyClass extends AppModel {
	
    public $useTable = 'classes';

    public $actsAs = array('Containable');

	public $order = array("MyClass.number" => "ASC", "MyClass.letter" => "ASC");

	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'Teacher' => array(
			'className' => 'Teacher',
			'foreignKey' => 'teacher_id',
			'conditions' => '',
			'fields' => ''
			// 'order' => ''
		)
	);
	
/*
	public function beforeFind($queryData) {
	    $sticky = array('letter' => 'ASC');
	
	    if (is_array($queryData['order'][0])) {
	        $queryData['order'][0] = $sticky + $queryData['order'][0];
	    }
	    else {
	        $queryData['order'][0] = $sticky;
	    }
	
	    return $queryData;
	}
*/

/**
 * hasMany associations
 *
 * @var array
 */

	public $hasMany = array(
		'Pupil' => array(
			'className' => 'Pupil',
			'foreignKey' => 'class_id'
			// 'dependent' => false,
			// 'conditions' => '',
			// 'fields' => '',
			,'order' => ['name_1', 'name_2', 'name_3'],
			// 'limit' => '',
			// 'offset' => '',
			// 'exclusive' => '',
			// 'finderQuery' => '',
			// 'counterQuery' => ''
		),
		'WorkTable' => array(
			'className' => 'WorkTable',
			'foreignKey' => 'class_id'
			// 'dependent' => false,
			// 'conditions' => '',
			// 'fields' => '',
			// ,'order' => ['name_1', 'name_2', 'name_3'],
			// 'limit' => '',
			// 'offset' => '',
			// 'exclusive' => '',
			// 'finderQuery' => '',
			// 'counterQuery' => ''
		)
	);

}
