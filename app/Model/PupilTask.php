<?php
App::uses('AppModel', 'Model');
/**
 * Mark Model
 *
 *
 *
 */
class PupilTask extends AppModel {

	public $actsAs = array('Containable');

	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'Pupil' => array(
			'className' => 'Pupil',
			'foreignKey' => 'pupil_id'
			// 'conditions' => '',
			// 'fields' => '',
			// 'order' => ''
		),
		'Task' => array(
			'className' => 'Task',
			'foreignKey' => 'task_id'
			// 'conditions' => '',
			// 'fields' => '',
			// 'order' => ''
		)
	);

}
