<?php
App::uses('AppModel', 'Model');
/**
 * Mark Model
 *
 *
 *
 */
class Subject extends AppModel {

	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'Type' => array(
			'className' => 'Type',
			'foreignKey' => 'type_id'
			// 'conditions' => '',
			// 'fields' => '',
			// 'order' => ''
		)
	);

}
