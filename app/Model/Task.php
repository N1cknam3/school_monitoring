<?php
App::uses('AppModel', 'Model');
/**
 * Task Model
 *
 *
 *
 */
class Task extends AppModel {

	public $actsAs = array('Containable');

	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * hasAndBelongsToMany associations
 *
 * @var array
 */
	public $hasAndBelongsToMany = array(
        'Pupil' =>
            array(
                'className' => 'Pupil',
                'joinTable' => 'pupil_tasks',
                'foreignKey' => 'task_id',
                'associationForeignKey' => 'pupil_id'
                // 'unique' => true,
                // 'conditions' => '',
                // 'fields' => '',
                // 'order' => '',
                // 'limit' => '',
                // 'offset' => '',
                // 'finderQuery' => '',
                // 'with' => ''
            )
    );


}
