<?php
App::uses('AppModel', 'Model');
/**
 * Class Model
 *
 *
 *
 */
class Type extends AppModel {

	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * hasMany associations
 *
 * @var array
 */
	public $hasMany = array(
		'Subject' => array(
			'className' => 'Subject',
			'foreignKey' => 'type_id',
			// 'dependent' => false,
			// 'conditions' => '',
			// 'fields' => '',
			'order' => array(
				'name' => 'ASC'
			)
			// 'limit' => '',
			// 'offset' => '',
			// 'exclusive' => '',
			// 'finderQuery' => '',
			// 'counterQuery' => ''
		)
	);

}
